//
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 28.10.2018.
//  Copyright © 2018 HKT. All rights reserved.
//  This code is mostly taken from the ref10 version of Curve25519 in SUPERCOP
//  (http://bench.cr.yp.to/supercop.html). which is released as public domain
//
//
#include "fe.h"

/*
h = 1
*/

void fe_1(fe h)
{
  h[0] = 1;
  h[1] = 0;
  h[2] = 0;
  h[3] = 0;
  h[4] = 0;
  h[5] = 0;
  h[6] = 0;
  h[7] = 0;
  h[8] = 0;
  h[9] = 0;
}

void fe_11(fe h)
{
    h[0] = 0x07FFFFFE;
    h[1] = 0x07FFFFFF;
    h[2] = 0x07FFFFFF;
    h[3] = 0x07FFFFFF;
    h[4] = 0x07FFFFFF;
    h[5] = 0x07FFFFFF;
    h[6] = 0x07FFFFFF;
    h[7] = 0x07FFFFFF;
    h[8] = 0x07FFFFFF;
    h[9] = 0x07FFFFF;
}
