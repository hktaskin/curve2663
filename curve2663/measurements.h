//
//  measurements.h
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 6.11.2018.
//  Copyright © 2018 HKT. All rights reserved.
//  https://github.com/vkrasnov/avx_qsort/blob/master/measurements.h

#ifndef measurements_h
#define measurements_h

#define REPEAT 100000
#define OUTER_REPEAT 1
#define WARMUP (REPEAT/4)

unsigned long long RDTSC_start_clk, RDTSC_end_clk;
double RDTSC_total_clk;
double RDTSC_TEMP_CLK;
int RDTSC_MEASURE_ITERATOR;
int RDTSC_OUTER_ITERATOR;

inline static unsigned long long get_Clks(void)
{
    unsigned hi, lo;
    __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
    return ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );
}

/*
 This MACRO measures the number of cycles "x" runs. This is the flow:
 1) it sets the priority to FIFO, to avoid time slicing if possible.
 2) it repeats "x" WARMUP times, in order to warm the cache.
 3) it reads the Time Stamp Counter at the beginning of the test.
 4) it repeats "x" REPEAT number of times.
 5) it reads the Time Stamp Counter again at the end of the test
 6) it calculates the average number of cycles per one iteration of "x", by calculating the total number of cycles, and dividing it by REPEAT
 */
#define RDTSC_MEASURE(x)                                                                         \
for(RDTSC_MEASURE_ITERATOR=0; RDTSC_MEASURE_ITERATOR< WARMUP; RDTSC_MEASURE_ITERATOR++)          \
{                                                                                             \
{x};                                                                                       \
}                                                                                    		    \
RDTSC_total_clk = 1.7976931348623157e+308;                                                      \
for(RDTSC_OUTER_ITERATOR=0;RDTSC_OUTER_ITERATOR<OUTER_REPEAT; RDTSC_OUTER_ITERATOR++){          \
RDTSC_start_clk = get_Clks();                                                                 \
for (RDTSC_MEASURE_ITERATOR = 0; RDTSC_MEASURE_ITERATOR < REPEAT; RDTSC_MEASURE_ITERATOR++)   \
{                                                                                             \
{x};                                                                                       \
}                                                                                             \
RDTSC_end_clk = get_Clks();                                                                   \
RDTSC_TEMP_CLK = (double)(RDTSC_end_clk-RDTSC_start_clk)/REPEAT;                              \
if(RDTSC_total_clk>RDTSC_TEMP_CLK) RDTSC_total_clk = RDTSC_TEMP_CLK;				        \
}

#define MEASURE RDTSC_MEASURE

#endif /* measurements_h */

