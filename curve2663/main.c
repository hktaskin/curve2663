//
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 28.10.2018.
//  Copyright © 2018 HKT. All rights reserved.
//  This code is mostly taken from the ref10 version of Curve25519 in SUPERCOP
//  (http://bench.cr.yp.to/supercop.html). which is released as public domain
//
//

#include <stdio.h>
#include <stdlib.h>
#include "test.h"

int main(int argc, const char * argv[]) {
    
    printf("HKT.me\n");
    
    //srandom(0xD3C0D33);
    
    /*
     for (int i = 0; i < 1; i++) {
     test_from_to_bytes(RANDOMIZE);
     printf("\n");
     }*/
    
    /*
     for (int i = 0; i < 1; i++) {
     test_fe_mul5177(RANDOMIZE);
     printf("\n");
     }*/
    
    /*
    for (int i = 0; i < 1; i++) {
        test_fe_mul(NOT_RANDOMIZE);
        printf("\n");
    }*/
    
    /*
    for (int i = 0; i < 1; i++) {
        test_fe_sq(NOT_RANDOMIZE);
        printf("\n");
    }*/
    
    /*
    for (int i = 0; i < 1; i++) {
        test_fe_invert(NOT_RANDOMIZE);
        printf("\n");
    }*/
    
    //test_ecdh(NOT_RANDOMIZE);
    
    // cycles
    test_cycles_fe_mul(NOT_RANDOMIZE);
    //test_cycles_fe_invert(NOT_RANDOMIZE);
    //test_cycles_ecdh(RANDOMIZE);
    
    return 0;
}
