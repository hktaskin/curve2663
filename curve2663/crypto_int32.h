//
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 28.10.2018.
//  Copyright © 2018 HKT. All rights reserved.
//  This code is mostly taken from the ref10 version of Curve25519 in SUPERCOP
//  (http://bench.cr.yp.to/supercop.html). which is released as public domain
//
//
#ifndef crypto_int32_H
#define crypto_int32_H

#include <stdint.h>

typedef int32_t crypto_int32;
typedef uint32_t crypto_uint32;


#endif
