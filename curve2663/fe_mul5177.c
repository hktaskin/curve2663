//
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 28.10.2018.
//  Copyright © 2018 HKT. All rights reserved.
//  This code is mostly taken from the ref10 version of Curve25519 in SUPERCOP
//  (http://bench.cr.yp.to/supercop.html). which is released as public domain
//
// DONE
//
#include "fe.h"
#include "crypto_uint64.h"

/*
 (20710 - 2) / 4 = 5177
 h = f * 5178
 Can overlap h with f.
 
 Preconditions:
 |f| bounded by 1.1*2^27,1.1*2^27,1.1*2^27,1.1*2^27,etc.
 
 Postconditions:
 |h| bounded by 1.1*2^27,1.1*2^27,1.1*2^27,1.1*2^27,etc.
 */

void fe_mul5177(fe h,fe f)
{
    crypto_uint64 h0 = ((crypto_uint64)f[0] * 5178LL);
    crypto_uint64 h1 = ((crypto_uint64)f[1] * 5178LL);
    crypto_uint64 h2 = ((crypto_uint64)f[2] * 5178LL);
    crypto_uint64 h3 = ((crypto_uint64)f[3] * 5178LL);
    crypto_uint64 h4 = ((crypto_uint64)f[4] * 5178LL);
    crypto_uint64 h5 = ((crypto_uint64)f[5] * 5178LL);
    crypto_uint64 h6 = ((crypto_uint64)f[6] * 5178LL);
    crypto_uint64 h7 = ((crypto_uint64)f[7] * 5178LL);
    crypto_uint64 h8 = ((crypto_uint64)f[8] * 5178LL);
    crypto_uint64 h9 = ((crypto_uint64)f[9] * 5178LL);
    crypto_uint64 carry;
    
    // Reduction
    carry = h0 >> 27; h0 &= cmask27; h1 += carry;
    carry = h1 >> 27; h1 &= cmask27; h2 += carry;
    carry = h2 >> 27; h2 &= cmask27; h3 += carry;
    carry = h3 >> 27; h3 &= cmask27; h4 += carry;
    carry = h4 >> 27; h4 &= cmask27; h5 += carry;
    carry = h5 >> 27; h5 &= cmask27; h6 += carry;
    carry = h6 >> 27; h6 &= cmask27; h7 += carry;
    carry = h7 >> 27; h7 &= cmask27; h8 += carry;
    carry = h8 >> 27; h8 &= cmask27; h9 += carry;
    carry = h9 >> 23; h9 &= cmask23; h0 += (carry * 3);
    carry = h0 >> 27; h0 &= cmask27; h1 += carry;
    
    h[0] = (crypto_int32) h0;
    h[1] = (crypto_int32) h1;
    h[2] = (crypto_int32) h2;
    h[3] = (crypto_int32) h3;
    h[4] = (crypto_int32) h4;
    h[5] = (crypto_int32) h5;
    h[6] = (crypto_int32) h6;
    h[7] = (crypto_int32) h7;
    h[8] = (crypto_int32) h8;
    h[9] = (crypto_int32) h9;
}
