//
//  fe_mul_tmvp.c
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 5.11.2018.
//  Copyright © 2018 HKT. All rights reserved.
//

#include <stdio.h>
#include "fe.h"
#include "crypto_int64.h"
#include "crypto_uint64.h"

crypto_uint64 P1, P2, P3, P4,
PF1, PF2, PF3, PF4, PF5, PF6, PF7, PF8,
PT1, PT2, PT3;

crypto_uint64 R[20];

// cmask27 = 0x7ffffff;
// cmask23 = 0x7fffff;

// T (2x2) * V (2x1) = M (2x1)
void Two_tmvp(crypto_uint64 *M0, crypto_uint64 *M1, crypto_uint64 T0, crypto_uint64 T1, crypto_uint64 T2, crypto_uint64 V0, crypto_uint64 V1)
{
    // 3M + 3A + 2Ad
    PT1 = T0 * (V0 + V1);
    PT2 = (T1 - T0) * V1;
    PT3 = (T2 - T0) * V0;
    *M0 = (PT1 + PT2);
    *M1 = (PT1 + PT3);
}

// T (2x2) * V (2x1) = M (2x1)
void Two_schoolbook(crypto_uint64 *M0, crypto_uint64 *M1, crypto_uint64 T0, crypto_uint64 T1, crypto_uint64 T2, crypto_uint64 V0, crypto_uint64 V1)
{
    // 4M + 2Ad
    *M0 = (T0 * V0);
    *M0 +=(T1 * V1);
    *M1 = (T2 * V0);
    *M1 +=(T0 * V1);
}

// T is TMVP (4x4) * V (4x1) = M (4x1)
// &MO,&M1,&M2,&M3
// T0, T1, T2, T3, T4, T5, T6
// V0, V1, V2, V3
void Four_by_four(crypto_uint64 *M0, crypto_uint64 *M1,crypto_uint64 *M2, crypto_uint64 *M3,
                  crypto_uint64 T0, crypto_uint64 T1,crypto_uint64 T2, crypto_uint64 T3,crypto_uint64 T4, crypto_uint64 T5,crypto_uint64 T6,
                  crypto_uint64 V0, crypto_uint64 V1,crypto_uint64 V2, crypto_uint64 V3)
{
    // *M0 = (T0*V0) + (T1*V1) + (T2*V2) + (T3*V3);
    // *M1 = (T4*V0) + (T0*V1) + (T1*V2) + (T2*V3);
    // *M2 = (T5*V0) + (T4*V1) + (T0*V2) + (T1*V3);
    // *M3 = (T6*V0) + (T5*V1) + (T4*V2) + (T0*V3);
    
#ifdef FE_MUL_TYPE_TMVP_SCHOOLBOOK
    Two_schoolbook(&PF1,&PF2,T0,T1,T4,V0,V1);
    Two_schoolbook(&PF3,&PF4,T5,T4,T6,V0,V1);
    Two_schoolbook(&PF5,&PF6,T2,T3,T1,V2,V3);
    Two_schoolbook(&PF7,&PF8,T0,T1,T4,V2,V3);
#elif defined FE_MUL_TYPE_TMVP_TMVP
    Two_tmvp(&PF1,&PF2,T0,T1,T4,V0,V1);
    Two_tmvp(&PF3,&PF4,T5,T4,T6,V0,V1);
    Two_tmvp(&PF5,&PF6,T2,T3,T1,V2,V3);
    Two_tmvp(&PF7,&PF8,T0,T1,T4,V2,V3);
#endif
    
    *M0 = (crypto_uint64)(PF1 + PF5);
    *M1 = (crypto_uint64)(PF2 + PF6);
    *M2 = (crypto_uint64)(PF3 + PF7);
    *M3 = (crypto_uint64)(PF4 + PF8);
}

void fe_mul_tmvp_new(fe h,fe f,fe g)
{
    //for (int i = 0; i < 20; i++) R[i] = 0;
    //P1 = 0; P2 = 0; P3 = 0; P4 = 0;
    //PT1 = 0; PT2 = 0; PT3 = 0;
    //PF1 = 0; PF2 = 0; PF3 = 0; PF4 = 0; PF5 = 0; PF6 = 0; PF7 = 0; PF8 = 0;
    
    // A0B0
    
    // T is TMVP (4x4) * V (4x1) = M (4x1)
    // &MO,&M1,&M2,&M3
    // T0, T1, T2, T3, T4, T5, T6
    // V0, V1, V2, V3
    Four_by_four(&R[1],&R[2],&R[3],&R[4],
                 (crypto_uint64)f[1],(crypto_uint64)f[0],0LL,0LL,(crypto_uint64)f[2],(crypto_uint64)f[3],(crypto_uint64)f[4],
                 (crypto_uint64)g[0],(crypto_uint64)g[1],(crypto_uint64)g[2],(crypto_uint64)g[3]);
    
    //printf("A\n");for (int i = 0; i < 20; i++) {printf("R[%2d] = %08llX %08llX\n",i,(R[i]>>32)&0xFFFFFFFF,R[i]&0xFFFFFFFF);} // DEBUG
    
    R[0] = ((crypto_uint64)f[0] * (crypto_uint64)g[0]);
    R[4] += ((crypto_uint64)f[0] * (crypto_uint64)g[4]);
    
    //printf("B: R0, R1, R2, R3, R4 should be same.\n");for (int i = 0; i < 20; i++) {printf("R[%2d] = %08llX %08llX\n",i,(R[i]>>32)&0xFFFFFFFF,R[i]&0xFFFFFFFF);} // DEBUG
    
    // DONE -> R[0],R[1],R[2],R[3],R[4]
    
    // A1B0
    Four_by_four(&R[5],&R[6],&R[7],&R[8],
                 (crypto_uint64)f[5],(crypto_uint64)f[4],(crypto_uint64)f[3],(crypto_uint64)f[2],(crypto_uint64)f[6],(crypto_uint64)f[7],(crypto_uint64)f[8],
                 (crypto_uint64)g[0],(crypto_uint64)g[1],(crypto_uint64)g[2],(crypto_uint64)g[3]);
    
    //printf("C\n");for (int i = 0; i < 20; i++) {printf("R[%2d] = %08llX %08llX\n",i,(R[i]>>32)&0xFFFFFFFFL,R[i]&0xFFFFFFFFL);} // DEBUG
    
    R[5] += ((crypto_uint64)f[1] * (crypto_uint64)g[4]);
    R[6] += ((crypto_uint64)f[2] * (crypto_uint64)g[4]);
    R[7] += ((crypto_uint64)f[3] * (crypto_uint64)g[4]);
    R[8] += ((crypto_uint64)f[4] * (crypto_uint64)g[4]);
    
    R[9]  = ((crypto_uint64)f[9] * (crypto_uint64)g[0]); //printf("R[9] = %08llX %08llX\n",(R[9]>>32)&0xFFFFFFFFL,R[9]&0xFFFFFFFFL);
    R[9] += ((crypto_uint64)f[8] * (crypto_uint64)g[1]); //printf("R[9] = %08llX %08llX\n",(R[9]>>32)&0xFFFFFFFFL,R[9]&0xFFFFFFFFL);
    R[9] += ((crypto_uint64)f[7] * (crypto_uint64)g[2]); //printf("R[9] = %08llX %08llX\n",(R[9]>>32)&0xFFFFFFFFL,R[9]&0xFFFFFFFFL);
    R[9] += ((crypto_uint64)f[6] * (crypto_uint64)g[3]); //printf("R[9] = %08llX %08llX\n",(R[9]>>32)&0xFFFFFFFFL,R[9]&0xFFFFFFFFL);
    R[9] += ((crypto_uint64)f[5] * (crypto_uint64)g[4]); //printf("R[9] = %08llX %08llX\n",(R[9]>>32)&0xFFFFFFFFL,R[9]&0xFFFFFFFFL);
    
    //printf("D\n");for (int i = 0; i < 20; i++) {printf("R[%2d] = %08llX %08llX\n",i,(R[i]>>32)&0xFFFFFFFF,R[i]&0xFFFFFFFF);} // DEBUG
    
    // A0B1
    Four_by_four(&P1,&P2,&P3,&P4,
                 (crypto_uint64)f[1],(crypto_uint64)f[0],0LL,0LL,(crypto_uint64)f[2],(crypto_uint64)f[3],(crypto_uint64)f[4],
                 (crypto_uint64)g[5],(crypto_uint64)g[6],(crypto_uint64)g[7],(crypto_uint64)g[8]);
    
    //printf("E\n");for (int i = 0; i < 20; i++) {printf("R[%2d] = %08llX %08llX\n",i,(R[i]>>32)&0xFFFFFFFF,R[i]&0xFFFFFFFF);} // DEBUG
    
    R[5] += ((crypto_uint64)f[0] * (crypto_uint64)g[5]);
    R[6] += (crypto_uint64)P1;
    R[7] += (crypto_uint64)P2;
    R[8] += (crypto_uint64)P3;
    R[9] += (crypto_uint64)P4;
    R[9] += ((crypto_uint64)f[0] * (crypto_uint64)g[9]);
    
    //printf("F: R5, R5, R7, R8, R9 should be same.\n");for (int i = 0; i < 20; i++) {printf("R[%2d] = %08llX %08llX\n",i,(R[i]>>32)&0xFFFFFFFF,R[i]&0xFFFFFFFF);} // DEBUG
    
    // DONE -> R[5],R[6],R[7],R[8],R[9]
    
    // A2B0
    Four_by_four(&R[10],&R[11],&R[12],&R[13],
                 (crypto_uint64)f[9],(crypto_uint64)f[8],(crypto_uint64)f[7],(crypto_uint64)f[6],0LL,0LL,0LL,
                 (crypto_uint64)g[1],(crypto_uint64)g[2],(crypto_uint64)g[3],(crypto_uint64)g[4]);
    
    //printf("G\n");for (int i = 0; i < 20; i++) {printf("R[%2d] = %08llX %08llX\n",i,(R[i]>>32)&0xFFFFFFFF,R[i]&0xFFFFFFFF);} // DEBUG
    
    // A1B1
    Four_by_four(&P1,&P2,&P3,&P4,
                 (crypto_uint64)f[5],(crypto_uint64)f[4],(crypto_uint64)f[3],(crypto_uint64)f[2],(crypto_uint64)f[6],(crypto_uint64)f[7],(crypto_uint64)f[8],
                 (crypto_uint64)g[5],(crypto_uint64)g[6],(crypto_uint64)g[7],(crypto_uint64)g[8]);
    
    //printf("H\n");for (int i = 0; i < 20; i++) {printf("R[%2d] = %08llX %08llX\n",i,(R[i]>>32)&0xFFFFFFFF,R[i]&0xFFFFFFFF);} // DEBUG
    
    R[10] += (crypto_uint64)P1;
    R[10] += ((crypto_uint64)f[1] * (crypto_uint64)g[9]);
    
    R[11] += (crypto_uint64)P2;
    R[11] += ((crypto_uint64)f[2] * (crypto_uint64)g[9]);
    
    R[12] += (crypto_uint64)P3;
    R[12] += ((crypto_uint64)f[3] * (crypto_uint64)g[9]);
    
    R[13] += (crypto_uint64)P4;
    R[13] += ((crypto_uint64)f[4] * (crypto_uint64)g[9]);
    
    R[14] =  ((crypto_uint64)f[9] * (crypto_uint64)g[5]);
    R[14] += ((crypto_uint64)f[8] * (crypto_uint64)g[6]);
    R[14] += ((crypto_uint64)f[7] * (crypto_uint64)g[7]);
    R[14] += ((crypto_uint64)f[6] * (crypto_uint64)g[8]);
    R[14] += ((crypto_uint64)f[5] * (crypto_uint64)g[9]);
    
    //printf("I: R10, R11, R12, R13, R14 should be same.\n");for (int i = 0; i < 20; i++) {printf("R[%2d] = %08llX %08llX\n",i,(R[i]>>32)&0xFFFFFFFF,R[i]&0xFFFFFFFF);} // DEBUG
    
    // DONE -> R[10],R[11],R[12],R[13],R[14]
    
    // A2B1
    Four_by_four(&R[15],&R[16],&R[17],&R[18],
                 f[9],f[8],f[7],f[6],0LL,0LL,0LL,
                 g[6],g[7],g[8],g[9]);
    
    R[19] = 0LL;
    
    //printf("J: R15, R16, R17, R18, R19 should be same.\n");for (int i = 0; i < 20; i++) {printf("R[%2d] = %08llX %08llX\n",i,(R[i]>>32)&0xFFFFFFFF,R[i]&0xFFFFFFFF);} // DEBUG

    // DONE -> R[15],R[16],R[17],R[18],R[19]
    
    //printf("K\n");for (int i = 0; i < 20; i++) {printf("R[%2d] = %08llX %08llX\n",i,(R[i]>>32)&0xFFFFFFFF,R[i]&0xFFFFFFFF);} // DEBUG
    
    R[0] += ((R[10] << 5) + (R[10] << 4));
    R[1] += ((R[11] << 5) + (R[11] << 4));
    R[2] += ((R[12] << 5) + (R[12] << 4));
    R[3] += ((R[13] << 5) + (R[13] << 4));
    R[4] += ((R[14] << 5) + (R[14] << 4));
    R[5] += ((R[15] << 5) + (R[15] << 4));
    R[6] += ((R[16] << 5) + (R[16] << 4));
    R[7] += ((R[17] << 5) + (R[17] << 4));
    R[8] += ((R[18] << 5) + (R[18] << 4));
    //R[9] += (R[19] << 5) + (R[19] << 4);
    
    //printf("L\n");for (int i = 0; i < 20; i++) {printf("R[%2d] = %08llX %08llX\n",i,(R[i]>>32)&0xFFFFFFFF,R[i]&0xFFFFFFFF);} // DEBUG
    
    // Reduction
    P1 = R[0] >> 27; R[0] &= cmask27; R[1] += P1;
    P1 = R[1] >> 27; R[1] &= cmask27; R[2] += P1;
    P1 = R[2] >> 27; R[2] &= cmask27; R[3] += P1;
    P1 = R[3] >> 27; R[3] &= cmask27; R[4] += P1;
    P1 = R[4] >> 27; R[4] &= cmask27; R[5] += P1;
    P1 = R[5] >> 27; R[5] &= cmask27; R[6] += P1;
    P1 = R[6] >> 27; R[6] &= cmask27; R[7] += P1;
    P1 = R[7] >> 27; R[7] &= cmask27; R[8] += P1;
    P1 = R[8] >> 27; R[8] &= cmask27; R[9] += P1;
    P1 = R[9] >> 23; R[9] &= cmask23; R[0] += (P1 * 3);
    P1 = R[0] >> 27; R[0] &= cmask27; R[1] += P1;
    
    /*if(R[0] == 0x7FFFFFE)
    {
        R[0] += 2;
        P1 = R[0] >> 27; R[0] &= cmask27; R[1] += P1;
        P1 = R[1] >> 27; R[1] &= cmask27; R[2] += P1;
        P1 = R[2] >> 27; R[2] &= cmask27; R[3] += P1;
        P1 = R[3] >> 27; R[3] &= cmask27; R[4] += P1;
        P1 = R[4] >> 27; R[4] &= cmask27; R[5] += P1;
        P1 = R[5] >> 27; R[5] &= cmask27; R[6] += P1;
        P1 = R[6] >> 27; R[6] &= cmask27; R[7] += P1;
        P1 = R[7] >> 27; R[7] &= cmask27; R[8] += P1;
        P1 = R[8] >> 27; R[8] &= cmask27; R[9] += P1;
        P1 = R[9] >> 23; R[9] &= cmask23; R[0] += P1;
        //P1 = R[0] >> 27; R[0] &= cmask27; R[1] += P1;
    }*/

    h[0] = (crypto_int32)R[0];
    h[1] = (crypto_int32)R[1];
    h[2] = (crypto_int32)R[2];
    h[3] = (crypto_int32)R[3];
    h[4] = (crypto_int32)R[4];
    h[5] = (crypto_int32)R[5];
    h[6] = (crypto_int32)R[6];
    h[7] = (crypto_int32)R[7];
    h[8] = (crypto_int32)R[8];
    h[9] = (crypto_int32)R[9];
}

//---### ARRAY IMPLEMENTATION ###---------------------------------------------------------------------------------------------------------//
/*
typedef crypto_uint64 T2RES[2];
typedef crypto_uint64 T2MAT[3];
typedef crypto_uint64 T2VEC[2];

T2RES t2res1,t2res2,t2res3,t2res4;
T2MAT t2mat1,t2mat2,t2mat3,t2mat4;
T2VEC t2vec1,t2vec2,t2vec3,t2vec4;

typedef crypto_uint64 T4RES[4];
typedef crypto_uint64 T4MAT[7];
typedef crypto_uint64 T4VEC[4];
*/
/*
// T (2x2) * V (2x1) = M (2x1)
void Two_tmvp_basic(T2RES M, const T2MAT T, const T2VEC V)
{
    // 3M + 3A + 2Ad
    PT1 = T[0] * (V[0] + V[1]);
    PT2 = (T[1] - T[0]) * V[1];
    PT3 = (T[2] - T[0]) * V[0];
    M[0] = (PT1 + PT2);
    M[1] = (PT1 + PT3);
}
// T (2x2) * V (2x1) = M (2x1)
void Two_schoolbook_basic(T2RES M, const T2MAT T, const T2VEC V)
{
    // 4M + 2Ad
    M[0] = (T[0] * V[0]);
    M[0] +=(T[1] * V[1]);
    M[1] = (T[2] * V[0]);
    M[1] +=(T[0] * V[1]);
}
// T is TMVP (4x4) * V (4x1) = M (4x1)
// &MO,&M1,&M2,&M3
// T0, T1, T2, T3, T4, T5, T6
// V0, V1, V2, V3
void Four_by_four_basic(T4RES M, const T4MAT T, const T4VEC V)
{
    // *M0 = (T0*V0) + (T1*V1) + (T2*V2) + (T3*V3);
    // *M1 = (T4*V0) + (T0*V1) + (T1*V2) + (T2*V3);
    // *M2 = (T5*V0) + (T4*V1) + (T0*V2) + (T1*V3);
    // *M3 = (T6*V0) + (T5*V1) + (T4*V2) + (T0*V3);
    
    t2mat1[0] = T[0]; t2mat1[1] = T[1]; t2mat1[2] = T[4];
    t2vec1[0] = V[0]; t2vec1[1] = V[1];
    
    t2mat2[0] = T[4]; t2mat2[1] = T[5]; t2mat2[2] = T[6];
    t2vec2[0] = V[0]; t2vec2[1] = V[1];
    
    t2mat3[0] = T[3]; t2mat3[1] = T[3]; t2mat3[2] = T[1];
    t2vec3[0] = V[2]; t2vec3[1] = V[3];
    
    t2mat4[0] = T[0]; t2mat4[1] = T[1]; t2mat4[2] = T[4];
    t2vec4[0] = V[2]; t2vec4[1] = V[3];
 
#ifdef FE_MUL_TYPE_TMVP_SCHOOLBOOK
    Two_schoolbook_basic(t2res1,t2mat1,t2vec1);
    Two_schoolbook_basic(t2res2,t2mat2,t2vec2);
    Two_schoolbook_basic(t2res3,t2mat3,t2vec3);
    Two_schoolbook_basic(t2res4,t2mat4,t2vec4);
#elif defined FE_MUL_TYPE_TMVP_TMVP
    Two_tmvp_basic(t2res1,t2mat1,t2vec1);
    Two_tmvp_basic(t2res2,t2mat2,t2vec2);
    Two_tmvp_basic(t2res3,t2mat3,t2vec3);
    Two_tmvp_basic(t2res4,t2mat4,t2vec4);
#endif
    
    M[0] = (crypto_uint64)(t2res1[0] + t2res3[0]);
    M[1] = (crypto_uint64)(t2res1[1] + t2res3[1]);
    M[2] = (crypto_uint64)(t2res2[0] + t2res4[0]);
    M[3] = (crypto_uint64)(t2res2[1] + t2res4[1]);
    
}
*/
// ##### OLD NOT WORKING IMPLEMENTATION #####
/*
void fe_mul_tmvp_old(fe h,fe f,fe g)
{
    
    crypto_uint64 m[10];
    crypto_uint64 n[10];
    crypto_uint64 c0[2]; crypto_uint64 c1[2];
    crypto_uint64 d0[2]; crypto_uint64 d1[2];
    crypto_uint64 t[24];
    crypto_uint64 v1[2]; crypto_uint64 v2[2]; crypto_uint64 v3[2];
    crypto_uint64 kl[4]; crypto_uint64 kn[4];
    crypto_uint64 pl[4]; crypto_uint64 pn[4];
    crypto_uint64 rl[4]; crypto_uint64 rn[4];
    crypto_uint64 carry0;
    crypto_uint64 carry1;
    crypto_uint64 carry2;
    crypto_uint64 carry3;
    crypto_uint64 carry4;
    crypto_uint64 carry5;
    crypto_uint64 carry6;
    crypto_uint64 carry7;
    crypto_uint64 carry8;
    crypto_uint64 carry9;
    crypto_uint64 cc0;
    crypto_uint64 cc1;
    crypto_uint64 cc2;
    crypto_uint64 cc3;
    crypto_uint64 cc4;
    crypto_uint64 cc5;
    crypto_uint64 cc6;
    crypto_uint64 cc7;
    crypto_uint64 cc8;
    crypto_uint64 cc9;
    
    m[0] = g[0] * f[0];
    m[4] = g[0] * f[4];
    m[5] = g[0] * f[5] + g[1] * f[4];
    m[6] = g[2] * f[4];
    m[7] = g[3] * f[4];
    m[8] = g[4] * f[4];
    m[9] = g[9] * f[0] + g[8] * f[1] + g[7] * f[2] + g[6] * f[3] + g[5] * f[4];
    
    n[0] = g[1] * f[9];
    n[1] = g[2] * f[9];
    n[2] = g[3] * f[9];
    n[3] = g[4] * f[9];
    n[4] = g[5] * f[9] + g[9] * f[5] + g[8] * f[6] + g[7] * f[7] + g[6] * f[8];
    n[9] = 0;
    
    c0[0] = f[0];
    c0[1] = f[1];
    c1[0] = f[2];
    c1[1] = f[3];
    
    t[1] = c0[0] + c1[0];
    t[2] = c0[1] + c1[1];
    
    t[3] = -g[1];
    t[4] = g[0] - g[2];
    t[5] = -g[0];
    t[7] = g[3] - g[1];
    t[8] = g[4] - g[2];
    t[9] = g[2] - g[0];
    
    t[11] = t[1] + t[2]; t[14] = g[1] * t[11];
    t[12] = g[0] - g[1]; t[15] = t[12] * t[2];
    t[13] = g[2] - g[1]; t[16] = t[13] * t[1];
    v1[0] = t[14] + t[15]; v1[1] = t[14] + t[16];
    
    t[19] = c1[0] + c1[1]; t[14] = t[3] * t[19];
    t[12] = t[5] - t[3]; t[15] = t[12] * c1[1];
    t[13] = t[4] - t[3]; t[16] = t[13] * c1[0];
    v2[0] = t[14] + t[15]; v2[1] = t[14] + t[16];
    
    t[20] = c0[0] + c0[1]; t[14] = t[7] * t[20];
    t[12] = t[9] - t[7]; t[15] = t[12] * c0[1];
    t[13] = t[8] - t[7]; t[16] = t[13] * c0[0];
    v3[0] = t[14] + t[15]; v3[1] = t[14] + t[16];
    
    kl[0] = v1[0] + v2[0]; kl[2] = v1[0] + v3[0];
    kl[1] = v1[1] + v2[1]; kl[3] = v1[1] + v3[1];
    
    d0[0] = f[5];
    d0[1] = f[6];
    d1[0] = f[7];
    d1[1] = f[8];
    
    t[17] = d0[0] + d1[0];
    t[18] = d0[1] + d1[1];
    
    t[21] = t[17] + t[18]; t[14] = g[1] * t[21];
    t[12] = g[0] - g[1]; t[15] = t[12] * t[18];
    t[13] = g[2] - g[1]; t[16] = t[13] * t[17];
    v1[0] = t[14] + t[15]; v1[1] = t[14] + t[16];
    
    t[22] = d1[0] + d1[1]; t[14] = t[3] * t[22];
    t[12] = t[5] - t[3]; t[15] = t[12] * d1[1];
    t[13] = t[4] - t[3]; t[16] = t[13] * d1[0];
    v2[0] = t[14] + t[15]; v2[1] = t[14] + t[16];
    
    t[23] = d0[0] + d0[1]; t[14] = t[7] * t[23];
    t[12] = t[9] - t[7]; t[15] = t[12] * d0[1];
    t[13] = t[8] - t[7]; t[16] = t[13] * d0[0];
    v3[0] = t[14] + t[15]; v3[1] = t[14] + t[16];
    
    kn[0] = v1[0] + v2[0]; kn[2] = v1[0] + v3[0];
    kn[1] = v1[1] + v2[1]; kn[3] = v1[1] + v3[1];
    
    t[3] = g[3] - g[5];
    t[5] = g[2] - g[4];
    t[4] = g[4] - g[6];
    t[7] = g[7] - g[5];
    t[9] = g[6] - g[4];
    t[8] = g[8] - g[6];
    
    t[14] = g[5] * t[11];
    t[12] = g[4] - g[5]; t[15] = t[12] * t[2];
    t[13] = g[6] - g[5]; t[16] = t[13] * t[1];
    v1[0] = t[14] + t[15]; v1[1] = t[14] + t[16];
    
    t[14] = t[3] * t[19];
    t[12] = t[5] - t[3]; t[15] = t[12] * c1[1];
    t[13] = t[4] - t[3]; t[16] = t[13] * c1[0];
    v2[0] = t[14] + t[15]; v2[1] = t[14] + t[16];
    
    t[14] = t[7] * t[20];
    t[12] = t[9] - t[7]; t[15] = t[12] * c0[1];
    t[13] = t[8] - t[7]; t[16] = t[13] * c0[0];
    v3[0] = t[14] + t[15]; v3[1] = t[14] + t[16];
    
    pl[0] = v1[0] + v2[0]; pl[2] = v1[0] + v3[0];
    pl[1] = v1[1] + v2[1]; pl[3] = v1[1] + v3[1];
    
    t[14] = g[5] * t[21];
    t[12] = g[4] - g[5]; t[15] = t[12] * t[18];
    t[13] = g[6] - g[5]; t[16] = t[13] * t[17];
    v1[0] = t[14] + t[15]; v1[1] = t[14] + t[16];
    
    t[14] = t[3] * t[22];
    t[12] = t[5] - t[3]; t[15] = t[12] * d1[1];
    t[13] = t[4] - t[3]; t[16] = t[13] * d1[0];
    v2[0] = t[14] + t[15]; v2[1] = t[14] + t[16];
    
    t[14] = t[7] * t[23];
    t[12] = t[9] - t[7]; t[15] = t[12] * d0[1];
    t[13] = t[8] - t[7]; t[16] = t[13] * d0[0];
    v3[0] = t[14] + t[15]; v3[1] = t[14] + t[16];
    
    pn[0] = v1[0] + v2[0]; pn[2] = v1[0] + v3[0];
    pn[1] = v1[1] + v2[1]; pn[3] = v1[1] + v3[1];
    
    c0[0] = f[1];
    c0[1] = f[2];
    c1[0] = f[3];
    c1[1] = f[4];
    
    t[1] = c0[0] + c1[0];
    t[2] = c0[1] + c1[1];
    t[3] = g[7] - g[9];
    t[5] = g[6] - g[8];
    
    t[11] = t[1] + t[2]; t[14] = g[9] * t[11];
    t[12] = g[8] - g[9]; t[15] = t[12] * t[2];
    t[13] = - g[9]; t[16] = t[13] * t[1];
    v1[0] = t[14] + t[15]; v1[1] = t[14] + t[16];
    
    t[19] = c1[0] + c1[1]; t[14] = t[3] * t[19];
    t[12] = t[5] - t[3]; t[15] = t[12] * c1[1];
    t[13] = g[8] - t[3]; t[16] = t[13] * c1[0];
    v2[0] = t[14] + t[15]; v2[1] = t[14] + t[16];
    
    t[20] = c0[0] + c0[1]; t[14] = -g[9] * t[20];
    t[12] = t[9] - t[8]; t[15] = t[12] * c0[1];
    t[13] = g[9]; t[16] = t[13] * c0[0];
    v3[0] = t[14] + t[15]; v3[1] = t[14] + t[16];
    
    rl[0] = v1[0] + v2[0]; rl[2] = v1[0] + v3[0];
    rl[1] = v1[1] + v2[1]; rl[3] = v1[1] + v3[1];
    
    d0[0] = f[6];
    d0[1] = f[7];
    d1[0] = f[8];
    d1[1] = f[9];
    
    t[17] = d0[0] + d1[0];
    t[18] = d0[1] + d1[1];
    
    t[21] = t[17] + t[18]; t[14] = g[9] * t[21];
    t[12] = g[8] - g[9]; t[15] = t[12] * t[18];
    t[13] = g[9]; t[16] = t[13] * t[17];
    v1[0] = t[14] + t[15]; v1[1] = t[14] + t[16];
    
    t[22] = d1[0] + d1[1]; t[14] = t[3] * t[22];
    t[12] = t[5] - t[3]; t[15] = t[12] * d1[1];
    t[13] = g[8] - t[3]; t[16] = t[13] * d1[0];
    v2[0] = t[14] + t[15]; v2[1] = t[14] + t[16];
    
    t[23] = d0[0] + d0[1]; t[14] = -g[9] * t[23];
    t[12] = g[9] - g[7]; t[15] = t[12] * d0[1];
    t[13] = g[9]; t[16] = t[13] * d0[0];
    v3[0] = t[14] + t[15]; v3[1] = t[14] + t[16];
    
    rn[0] = v1[0] + v2[0]; rn[2] = v1[0] + v3[0];
    rn[1] = v1[1] + v2[1]; rn[3] = v1[1] + v3[1];
    
    m[1] = kl[0];
    m[2] = kl[1];
    m[3] = kl[2];
    m[4] += kl[3];
    m[5] += pl[0];
    m[6] += pl[1] + kn[0];
    m[7] += pl[2] + kn[1];
    m[8] += pl[3] + kn[2];
    m[9] += kn[3];
    
    n[0] += rl[0] + pn[0];
    n[1] += rl[1] + pn[1];
    n[2] += rl[2] + pn[2];
    n[3] += rl[3] + pn[3];
    n[5] = rn[0];
    n[6] = rn[1];
    n[7] = rn[2];
    n[8] = rn[3];
    
    // x48 = 2^5 + 2^4
    
    cc0 = m[0] + (n[0] << 5) + (n[0] << 4);
    cc1 = m[1] + (n[1] << 5) + (n[1] << 4);
    cc2 = m[2] + (n[2] << 5) + (n[2] << 4);
    cc3 = m[3] + (n[3] << 5) + (n[3] << 4);
    cc4 = m[4] + (n[4] << 5) + (n[4] << 4);
    cc5 = m[5] + (n[5] << 5) + (n[5] << 4);
    cc6 = m[6] + (n[6] << 5) + (n[6] << 4);
    cc7 = m[7] + (n[7] << 5) + (n[7] << 4);
    cc8 = m[8] + (n[8] << 5) + (n[8] << 4);
    cc9 = m[9] + (n[9] << 5) + (n[9] << 4);
    
    carry0 = cc0 >> 27; cc1 += carry0; cc0 -= (carry0 << 27);
    carry1 = cc1 >> 27; cc2 += carry1; cc1 -= (carry1 << 27);
    carry2 = cc2 >> 27; cc3 += carry2; cc2 -= (carry2 << 27);
    carry3 = cc3 >> 27; cc4 += carry3; cc3 -= (carry3 << 27);
    carry4 = cc4 >> 27; cc5 += carry4; cc4 -= (carry4 << 27);
    carry5 = cc5 >> 27; cc6 += carry5; cc5 -= (carry5 << 27);
    carry6 = cc6 >> 27; cc7 += carry6; cc6 -= (carry6 << 27);
    carry7 = cc7 >> 27; cc8 += carry7; cc7 -= (carry7 << 27);
    carry8 = cc8 >> 27; cc9 += carry8; cc8 -= (carry8 << 27);
    carry9 = cc9 >> 23; cc0 += carry9 *3; cc9 -= (carry9 << 23);
    carry0 = cc0 >> 27; cc1 += carry0; cc0 -= (carry0 << 27);
    
    
    h[0] = (crypto_int32)cc0;
    h[1] = (crypto_int32)cc1;
    h[2] = (crypto_int32)cc2;
    h[3] = (crypto_int32)cc3;
    h[4] = (crypto_int32)cc4;
    h[5] = (crypto_int32)cc5;
    h[6] = (crypto_int32)cc6;
    h[7] = (crypto_int32)cc7;
    h[8] = (crypto_int32)cc8;
    h[9] = (crypto_int32)cc9;
}
*/

void fe_mul_tmvp(fe h,fe f,fe g)
{
    fe_mul_tmvp_new(h,f, g);
}
