//
//  fe_mul_donna.c
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 5.11.2018.
//  Copyright © 2018 HKT. All rights reserved.
//  https://github.com/agl/curve25519-donna/blob/master/curve25519-donna.c

#include <stdio.h>
#include "fe.h"
#include "crypto_int64.h"

void fe_mul_donna(fe h,fe f,fe g)
{
    crypto_int64 output[19];
    crypto_int64 carry;
    
    output[0] = ((crypto_int64)f[0] * (crypto_int64)g[0]);
    output[1] = ((crypto_int64)f[0] * g[1] + (crypto_int64)f[1] * g[0]);
    output[2] = ((crypto_int64)f[1] * g[1] + (crypto_int64)f[0] * g[2] + (crypto_int64)f[2] * g[0]);
    output[3] = ((crypto_int64)f[1] * g[2] + (crypto_int64)f[2] * g[1] + (crypto_int64)f[0] * g[3] + (crypto_int64)f[3] * g[0]);
    output[4] = ((crypto_int64)f[2] * g[2] + (crypto_int64)f[1] * g[3] + (crypto_int64)f[3] * g[1] + (crypto_int64)f[0] * g[4] + (crypto_int64)f[4] * g[0]);
    output[5] = ((crypto_int64)f[2] * g[3] + (crypto_int64)f[3] * g[2] + (crypto_int64)f[1] * g[4] + (crypto_int64)f[4] * g[1] + (crypto_int64)f[0] * g[5] + (crypto_int64)f[5] * g[0]);
    output[6] = ((crypto_int64)f[3] * g[3] + (crypto_int64)f[1] * g[5] + (crypto_int64)f[5] * g[1] + (crypto_int64)f[2] * g[4] + (crypto_int64)f[4] * g[2] + (crypto_int64)f[0] * g[6] + (crypto_int64)f[6] * g[0]);
    output[7] = ((crypto_int64)f[3] * g[4] + (crypto_int64)f[4] * g[3] + (crypto_int64)f[2] * g[5] + (crypto_int64)f[5] * g[2] + (crypto_int64)f[1] * g[6] + (crypto_int64)f[6] * g[1] + (crypto_int64)f[0] * g[7] + (crypto_int64)f[7] * g[0]);
    output[8] = ((crypto_int64)f[4] * g[4] + (crypto_int64)f[3] * g[5] + (crypto_int64)f[5] * g[3] + (crypto_int64)f[1] * g[7] + (crypto_int64)f[7] * g[1] + (crypto_int64)f[2] * g[6] + (crypto_int64)f[6] * g[2] + (crypto_int64)f[0] * g[8] + (crypto_int64)f[8] * g[0]);
    output[9] = ((crypto_int64)f[4] * g[5] + (crypto_int64)f[5] * g[4] + (crypto_int64)f[3] * g[6] + (crypto_int64)f[6] * g[3] + (crypto_int64)f[2] * g[7] + (crypto_int64)f[7] * g[2] + (crypto_int64)f[1] * g[8] + (crypto_int64)f[8] * g[1] + (crypto_int64)f[0] * g[9] + (crypto_int64)f[9] * g[0]);
    
    output[10] = ((crypto_int64)f[5] * g[5] + (crypto_int64)f[3] * g[7] + (crypto_int64)f[7] * g[3] + (crypto_int64)f[1] * g[9] + (crypto_int64)f[9] * g[1] + (crypto_int64)f[4] * g[6] + (crypto_int64)f[6] * g[4] + (crypto_int64)f[2] * g[8] + (crypto_int64)f[8] * g[2]);
    output[11] = ((crypto_int64)f[5] * g[6] + (crypto_int64)f[6] * g[5] + (crypto_int64)f[4] * g[7] + (crypto_int64)f[7] * g[4] + (crypto_int64)f[3] * g[8] + (crypto_int64)f[8] * g[3] + (crypto_int64)f[2] * g[9] + (crypto_int64)f[9] * g[2]);
    output[12] = ((crypto_int64)f[6] * g[6] + (crypto_int64)f[5] * g[7] + (crypto_int64)f[7] * g[5] + (crypto_int64)f[3] * g[9] + (crypto_int64)f[9] * g[3] + (crypto_int64)f[4] * g[8] + (crypto_int64)f[8] * g[4]);
    output[13] = ((crypto_int64)f[6] * g[7] + (crypto_int64)f[7] * g[6] + (crypto_int64)f[5] * g[8] + (crypto_int64)f[8] * g[5] + (crypto_int64)f[4] * g[9] + (crypto_int64)f[9] * g[4]);
    output[14] = ((crypto_int64)f[7] * g[7] + (crypto_int64)f[5] * g[9] + (crypto_int64)f[9] * g[5] + (crypto_int64)f[6] * g[8] + (crypto_int64)f[8] * g[6]);
    output[15] = ((crypto_int64)f[7] * g[8] + (crypto_int64)f[8] * g[7] + (crypto_int64)f[6] * g[9] + (crypto_int64)f[9] * g[6]);
    output[16] = ((crypto_int64)f[8] * g[8] + (crypto_int64)f[7] * g[9] + (crypto_int64)f[9] * g[7]);
    output[17] = ((crypto_int64)f[8] * g[9] + (crypto_int64)f[9] * g[8]);
    output[18] = ((crypto_int64)f[9] * g[9]);
    
    // multiply with 48 = 2^5 + 2^4
    
    output[8] += output[18] << 4;
    output[8] += output[18] << 5;
    
    output[7] += output[17] << 4;
    output[7] += output[17] << 5;
    
    output[6] += output[16] << 4;
    output[6] += output[16] << 5;

    output[5] += output[15] << 4;
    output[5] += output[15] << 5;

    output[4] += output[14] << 4;
    output[4] += output[14] << 5;

    output[3] += output[13] << 4;
    output[3] += output[13] << 5;

    output[2] += output[12] << 4;
    output[2] += output[12] << 5;

    output[1] += output[11] << 4;
    output[1] += output[11] << 5;

    output[0] += output[10] << 4;
    output[0] += output[10] << 5;
    
    // reduction
    carry = output[0] >> 27; output[1] += carry; output[0] -= (carry << 27);
    carry = output[1] >> 27; output[2] += carry; output[1] -= (carry << 27);
    carry = output[2] >> 27; output[3] += carry; output[2] -= (carry << 27);
    carry = output[3] >> 27; output[4] += carry; output[3] -= (carry << 27);
    carry = output[4] >> 27; output[5] += carry; output[4] -= (carry << 27);
    carry = output[5] >> 27; output[6] += carry; output[5] -= (carry << 27);
    carry = output[6] >> 27; output[7] += carry; output[6] -= (carry << 27);
    carry = output[7] >> 27; output[8] += carry; output[7] -= (carry << 27);
    carry = output[8] >> 27; output[9] += carry; output[8] -= (carry << 27);
    carry = output[9] >> 23; output[0] += carry *3; output[9] -= (carry << 23);
    carry = output[0] >> 27; output[1] += carry; output[0] -= (carry << 27);
    
    h[0] = (crypto_int32)output[0];
    h[1] = (crypto_int32)output[1];
    h[2] = (crypto_int32)output[2];
    h[3] = (crypto_int32)output[3];
    h[4] = (crypto_int32)output[4];
    h[5] = (crypto_int32)output[5];
    h[6] = (crypto_int32)output[6];
    h[7] = (crypto_int32)output[7];
    h[8] = (crypto_int32)output[8];
    h[9] = (crypto_int32)output[9];
}