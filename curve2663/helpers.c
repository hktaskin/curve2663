//
//  helpers.c
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 6.11.2018.
//  Copyright © 2018 HKT. All rights reserved.
//

#include "helpers.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

const fe mask927123 = {0xF8000000,0xF8000000,0xF8000000,0xF8000000,0xF8000000,0xF8000000,0xF8000000,0xF8000000,0xF8000000,0xFF800000};

void fail(const char *why)
{
    printf("%s\n",why);
    exit(111);
}

void printarray(const unsigned char *arr, int len)
{
    for (int i = 0; i < len; i++) {
        printf("%02X ",arr[i]);
    }
    printf("\n");
}

void comparearray(const unsigned char *arr1,const unsigned char *arr2, int len)
{
    //printf("Comparison: ");
    for (int i = 0; i < len; i++) {
        printf("%s  ",arr1[i] == arr2[i] ? "." : "X");
    }
    printf("\n");
}

void printfe(fe elt)
{
    for (int i = 0; i < 10; i++) {
        printf("%08X ",elt[i]);
    }
    printf("\n");
}

/*
void printfe_inverse(fe elt)
{
    for (int i = 9; i >= 0; i--) {
        printf("%08X ",elt[i]);
    }
    printf("\n");
}*/

void fecheck(fe elt)
{
    for (int i = 0; i < 10; i++) {
        printf("%s        ",(elt[i] & mask927123[i]) == 0  ? "." : "X");
    }
    printf("\n");
}

void fillrandombytes(unsigned char *x,unsigned long long xlen)
{
    for (int i = 0;i < xlen;i++) x[i] = (random() % 256) & 0xFF;
}

unsigned char *alignedcalloc(unsigned long long len)
{
    unsigned char *x = (unsigned char *) calloc(1,len + 256);
    long long i;
    if (!x) fail("out of memory");
    /* will never deallocate so shifting is ok */
    for (i = 0;i < len + 256;++i) x[i] = random();
    x += 64;
    x += 63 & (-(unsigned long) x);
    for (i = 0;i < len;++i) x[i] = 0;
    return x;
}


