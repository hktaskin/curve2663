//
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 28.10.2018.
//  Copyright © 2018 HKT. All rights reserved.
//  This code is mostly taken from the ref10 version of Curve25519 in SUPERCOP
//  (http://bench.cr.yp.to/supercop.html). which is released as public domain
//
//
#include "fe.h"
#include "crypto_int64.h"

void fe_frombytes(fe h,const unsigned char *s)
{
    crypto_int32 h0  = s[0];                                /*8*/
    h0 |= ((crypto_int32)s[1] << 8);                       /*16*/
    h0 |= ((crypto_int32)s[2] << 16);                      /*24*/
    h0 |= (((crypto_int32)(s[3]&0x07)) << 24);             /*27*/
    
    crypto_int32 h1  = ((crypto_int32)(s[3]&0xf8)>>3);		/*5*/
    h1 |= ((crypto_int32)s[4] << 5);                       /*13*/
    h1 |= ((crypto_int32)s[5] << 13);                      /*21*/
    h1 |= (((crypto_int32)(s[6])&0x3f)<< 21);              /*27*/
    
    crypto_int32 h2  = (((crypto_int32)(s[6])&0xc0)>> 6);	/*2*/
    h2 |= ((crypto_int32)s[7] << 2);                       /*10*/
    h2 |= ((crypto_int32)s[8] << 10);                      /*18*/
    h2 |= ((crypto_int32)s[9] << 18);                      /*26*/
    h2 |= (((crypto_int32)(s[10]&0x01)) << 26);            /*27*/
    
    crypto_int32 h3  = ((crypto_int32)(s[10]&0xfe)>>1);	/*7*/
    h3 |= ((crypto_int32)s[11] << 7);                      /*15*/
    h3 |= ((crypto_int32)s[12] << 15);                     /*23*/
    h3 |= (((crypto_int32)s[13]&0x0f)<< 23);               /*27*/
    
    crypto_int32 h4  = (((crypto_int32)s[13]&0xf0)>>4);	/*4*/
    h4 |= ((crypto_int32)s[14] << 4);                      /*12*/
    h4 |= ((crypto_int32)s[15] << 12);                     /*20*/
    h4 |= (((crypto_int32)(s[16]&0x7f)) << 20);            /*27*/
    
    crypto_int32 h5  = ((crypto_int32)(s[16]&0x80)>>7);    /*1*/
    h5 |= ((crypto_int32)s[17] << 1);                      /*9*/
    h5 |= ((crypto_int32)s[18] << 9);                      /*17*/
    h5 |= ((crypto_int32)(s[19])<< 17);                    /*25*/
    h5 |= (((crypto_int32)(s[20])&0x03)<< 25);             /*27*/
    
    crypto_int32 h6  = (((crypto_int32)(s[20])&0xfc)>>2);	/*6*/
    h6 |= ((crypto_int32)s[21] << 6);                      /*14*/
    h6 |= ((crypto_int32)s[22] << 14);                     /*22*/
    h6 |= (((crypto_int32)(s[23]&0x1f)) << 22);            /*27*/
    
    crypto_int32 h7  = ((crypto_int32)(s[23]&0xe0)>>5);	/*3*/
    h7 |= ((crypto_int32)s[24] << 3);                      /*11*/
    h7 |= ((crypto_int32)s[25] << 11);                     /*19*/
    h7 |= ((crypto_int32)s[26]<< 19);                      /*27*/
    
    crypto_int32 h8  = s[27];                               /*8*/
    h8 |= ((crypto_int32)s[28] << 8);                      /*16*/
    h8 |= ((crypto_int32)s[29] << 16);                     /*24*/
    h8 |= (((crypto_int32)(s[30]&0x07)) << 24);            /*27*/
    
    crypto_int32 h9  = ((crypto_int32)(s[30]&0xf8)>>3);    /*5*/
    h9 |= ((crypto_int32)s[31] << 5);                      /*13*/
    h9 |= ((crypto_int32)s[32] << 13);                     /*21*/
    h9 |= (((crypto_int32)(s[33])&0x03)<< 21);             /*23*/
    
    h[0] = h0;
    h[1] = h1;
    h[2] = h2;
    h[3] = h3;
    h[4] = h4;
    h[5] = h5;
    h[6] = h6;
    h[7] = h7;
    h[8] = h8;
    h[9] = h9;
}
