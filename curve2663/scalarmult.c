//
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 28.10.2018.
//  Copyright © 2018 HKT. All rights reserved.
//  This code is mostly taken from the ref10 version of Curve25519 in SUPERCOP
//  (http://bench.cr.yp.to/supercop.html). which is released as public domain
//
//
#include <stdio.h>
#include "fe.h"

int crypto_scalarmult(unsigned char *q,
                      const unsigned char *n,
                      const unsigned char *p)
{
    unsigned char e[34];
    unsigned int i;
    fe x1;
    fe x2;
    fe z2;
    fe x3;
    fe z3;
    fe tmp0;
    fe tmp1;
    int pos;
    unsigned int swap;
    unsigned int b;
    
    for (i = 0;i < 34;++i) e[i] = n[i];
    //e[0] &= 248; // lsb 3 bits 0
    //e[31] &= 127; // msb 0
    //e[31] |= 64; // top bit 1
    fe_frombytes(x1,p);
    fe_1(x2);
    fe_0(z2);
    fe_copy(x3,x1);
    fe_1(z3);
    
    swap = 0;
    for (pos = 265;pos >= 0;--pos) {
        b = e[pos / 8] >> (pos & 7);
        b &= 1;
        swap ^= b;
        fe_cswap(x2,x3,swap);
        fe_cswap(z2,z3,swap);
        swap = b;
        #include "montgomery.h"
    }
    fe_cswap(x2,x3,swap);
    fe_cswap(z2,z3,swap);
    
    fe_invert(z2,z2);
    fe_mul(x2,x2,z2);
    fe_tobytes(q,x2);
    return 0;
}

int crypto_scalarmult_base(unsigned char *q,const unsigned char *n)
{
    return crypto_scalarmult(q,n,basepoint);
}

