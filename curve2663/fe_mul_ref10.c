//
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 28.10.2018.
//  Copyright © 2018 HKT. All rights reserved.
//  This code is mostly taken from the ref10 version of Curve25519 in SUPERCOP
//  (http://bench.cr.yp.to/supercop.html). which is released as public domain
//
//
#include <stdio.h>
#include "fe.h"
#include "crypto_int64.h"
#include "crypto_uint64.h"

/*
 h = f * g
 Can overlap h with f or g.
 
 Preconditions:
 |f| bounded by 1.1*2^26,1.1*2^25,1.1*2^26,1.1*2^25,etc.
 |g| bounded by 1.1*2^26,1.1*2^25,1.1*2^26,1.1*2^25,etc.
 
 Postconditions:
 |h| bounded by 1.1*2^25,1.1*2^24,1.1*2^25,1.1*2^24,etc.
 */

/*
 Notes on implementation strategy:
 
 Using schoolbook multiplication.
 Karatsuba would save a little in some cost models.
 
 Most multiplications by 2 and 19 are 32-bit precomputations;
 cheaper than 64-bit postcomputations.
 
 There is one remaining multiplication by 19 in the carry chain;
 one *19 precomputation can be merged into this,
 but the resulting data flow is considerably less clean.
 
 There are 12 carries below.
 10 of them are 2-way parallelizable and vectorizable.
 Can get away with 11 carries, but then data flow is much deeper.
 
 With tighter constraints on inputs can squeeze carries into int32.
 */

void fe_mul_ref10(fe h,fe f,fe g)
{
    crypto_int32 f0 = f[0];
    crypto_int32 f1 = f[1];
    crypto_int32 f2 = f[2];
    crypto_int32 f3 = f[3];
    crypto_int32 f4 = f[4];
    crypto_int32 f5 = f[5];
    crypto_int32 f6 = f[6];
    crypto_int32 f7 = f[7];
    crypto_int32 f8 = f[8];
    crypto_int32 f9 = f[9];
    crypto_int32 g0 = g[0];
    crypto_int32 g1 = g[1];
    crypto_int32 g2 = g[2];
    crypto_int32 g3 = g[3];
    crypto_int32 g4 = g[4];
    crypto_int32 g5 = g[5];
    crypto_int32 g6 = g[6];
    crypto_int32 g7 = g[7];
    crypto_int32 g8 = g[8];
    crypto_int32 g9 = g[9];
    crypto_int64 g1_48 = 48 * (crypto_int64)g1;
    crypto_int64 g2_48 = 48 * (crypto_int64)g2;
    crypto_int64 g3_48 = 48 * (crypto_int64)g3;
    crypto_int64 g4_48 = 48 * (crypto_int64)g4;
    crypto_int64 g5_48 = 48 * (crypto_int64)g5;
    crypto_int64 g6_48 = 48 * (crypto_int64)g6;
    crypto_int64 g7_48 = 48 * (crypto_int64)g7;
    crypto_int64 g8_48 = 48 * (crypto_int64)g8;
    crypto_int64 g9_48 = 48 * (crypto_int64)g9;
    crypto_int64 f0g0    = f0   * (crypto_int64) g0;
    crypto_int64 f0g1    = f0   * (crypto_int64) g1;
    crypto_int64 f0g2    = f0   * (crypto_int64) g2;
    crypto_int64 f0g3    = f0   * (crypto_int64) g3;
    crypto_int64 f0g4    = f0   * (crypto_int64) g4;
    crypto_int64 f0g5    = f0   * (crypto_int64) g5;
    crypto_int64 f0g6    = f0   * (crypto_int64) g6;
    crypto_int64 f0g7    = f0   * (crypto_int64) g7;
    crypto_int64 f0g8    = f0   * (crypto_int64) g8;
    crypto_int64 f0g9    = f0   * (crypto_int64) g9;
    
    crypto_int64 f1g0    = f1   * (crypto_int64) g0;
    crypto_int64 f1g1    = f1   * (crypto_int64) g1;
    crypto_int64 f1g2    = f1   * (crypto_int64) g2;
    crypto_int64 f1g3    = f1   * (crypto_int64) g3;
    crypto_int64 f1g4    = f1   * (crypto_int64) g4;
    crypto_int64 f1g5    = f1   * (crypto_int64) g5;
    crypto_int64 f1g6    = f1   * (crypto_int64) g6;
    crypto_int64 f1g7    = f1   * (crypto_int64) g7;
    crypto_int64 f1g8    = f1   * (crypto_int64) g8;
    crypto_int64 f1g9_48 = f1   * (crypto_int64) g9_48;
    
    crypto_int64 f2g0    = f2   * (crypto_int64) g0;
    crypto_int64 f2g1    = f2   * (crypto_int64) g1;
    crypto_int64 f2g2    = f2   * (crypto_int64) g2;
    crypto_int64 f2g3    = f2   * (crypto_int64) g3;
    crypto_int64 f2g4    = f2   * (crypto_int64) g4;
    crypto_int64 f2g5    = f2   * (crypto_int64) g5;
    crypto_int64 f2g6    = f2   * (crypto_int64) g6;
    crypto_int64 f2g7    = f2   * (crypto_int64) g7;
    crypto_int64 f2g8_48 = f2   * (crypto_int64) g8_48;
    crypto_int64 f2g9_48 = f2   * (crypto_int64) g9_48;
    
    crypto_int64 f3g0    = f3   * (crypto_int64) g0;
    crypto_int64 f3g1    = f3   * (crypto_int64) g1;
    crypto_int64 f3g2    = f3   * (crypto_int64) g2;
    crypto_int64 f3g3    = f3   * (crypto_int64) g3;
    crypto_int64 f3g4    = f3   * (crypto_int64) g4;
    crypto_int64 f3g5    = f3   * (crypto_int64) g5;
    crypto_int64 f3g6    = f3   * (crypto_int64) g6;
    crypto_int64 f3g7_48 = f3   * (crypto_int64) g7_48;
    crypto_int64 f3g8_48 = f3   * (crypto_int64) g8_48;
    crypto_int64 f3g9_48 = f3   * (crypto_int64) g9_48;
    
    crypto_int64 f4g0    = f4   * (crypto_int64) g0;
    crypto_int64 f4g1    = f4   * (crypto_int64) g1;
    crypto_int64 f4g2    = f4   * (crypto_int64) g2;
    crypto_int64 f4g3    = f4   * (crypto_int64) g3;
    crypto_int64 f4g4    = f4   * (crypto_int64) g4;
    crypto_int64 f4g5    = f4   * (crypto_int64) g5;
    crypto_int64 f4g6_48 = f4   * (crypto_int64) g6_48;
    crypto_int64 f4g7_48 = f4   * (crypto_int64) g7_48;
    crypto_int64 f4g8_48 = f4   * (crypto_int64) g8_48;
    crypto_int64 f4g9_48 = f4   * (crypto_int64) g9_48;
    
    crypto_int64 f5g0    = f5   * (crypto_int64) g0;
    crypto_int64 f5g1    = f5   * (crypto_int64) g1;
    crypto_int64 f5g2    = f5   * (crypto_int64) g2;
    crypto_int64 f5g3    = f5   * (crypto_int64) g3;
    crypto_int64 f5g4    = f5   * (crypto_int64) g4;
    crypto_int64 f5g5_48 = f5   * (crypto_int64) g5_48;
    crypto_int64 f5g6_48 = f5   * (crypto_int64) g6_48;
    crypto_int64 f5g7_48 = f5   * (crypto_int64) g7_48;
    crypto_int64 f5g8_48 = f5   * (crypto_int64) g8_48;
    crypto_int64 f5g9_48 = f5   * (crypto_int64) g9_48;
    
    crypto_int64 f6g0    = f6   * (crypto_int64) g0;
    crypto_int64 f6g1    = f6   * (crypto_int64) g1;
    crypto_int64 f6g2    = f6   * (crypto_int64) g2;
    crypto_int64 f6g3    = f6   * (crypto_int64) g3;
    crypto_int64 f6g4_48 = f6   * (crypto_int64) g4_48;
    crypto_int64 f6g5_48 = f6   * (crypto_int64) g5_48;
    crypto_int64 f6g6_48 = f6   * (crypto_int64) g6_48;
    crypto_int64 f6g7_48 = f6   * (crypto_int64) g7_48;
    crypto_int64 f6g8_48 = f6   * (crypto_int64) g8_48;
    crypto_int64 f6g9_48 = f6   * (crypto_int64) g9_48;
    
    crypto_int64 f7g0    = f7   * (crypto_int64) g0;
    crypto_int64 f7g1    = f7   * (crypto_int64) g1;
    crypto_int64 f7g2    = f7   * (crypto_int64) g2;
    crypto_int64 f7g3_48 = f7   * (crypto_int64) g3_48;
    crypto_int64 f7g4_48 = f7   * (crypto_int64) g4_48;
    crypto_int64 f7g5_48 = f7   * (crypto_int64) g5_48;
    crypto_int64 f7g6_48 = f7   * (crypto_int64) g6_48;
    crypto_int64 f7g7_48 = f7   * (crypto_int64) g7_48;
    crypto_int64 f7g8_48 = f7   * (crypto_int64) g8_48;
    crypto_int64 f7g9_48 = f7   * (crypto_int64) g9_48;
    
    crypto_int64 f8g0    = f8   * (crypto_int64) g0;
    crypto_int64 f8g1    = f8   * (crypto_int64) g1;
    crypto_int64 f8g2_48 = f8   * (crypto_int64) g2_48;
    crypto_int64 f8g3_48 = f8   * (crypto_int64) g3_48;
    crypto_int64 f8g4_48 = f8   * (crypto_int64) g4_48;
    crypto_int64 f8g5_48 = f8   * (crypto_int64) g5_48;
    crypto_int64 f8g6_48 = f8   * (crypto_int64) g6_48;
    crypto_int64 f8g7_48 = f8   * (crypto_int64) g7_48;
    crypto_int64 f8g8_48 = f8   * (crypto_int64) g8_48;
    crypto_int64 f8g9_48 = f8   * (crypto_int64) g9_48;
    
    crypto_int64 f9g0    = f9   * (crypto_int64) g0;
    crypto_int64 f9g1_48 = f9   * (crypto_int64) g1_48;
    crypto_int64 f9g2_48 = f9   * (crypto_int64) g2_48;
    crypto_int64 f9g3_48 = f9   * (crypto_int64) g3_48;
    crypto_int64 f9g4_48 = f9   * (crypto_int64) g4_48;
    crypto_int64 f9g5_48 = f9   * (crypto_int64) g5_48;
    crypto_int64 f9g6_48 = f9   * (crypto_int64) g6_48;
    crypto_int64 f9g7_48 = f9   * (crypto_int64) g7_48;
    crypto_int64 f9g8_48 = f9   * (crypto_int64) g8_48;
    crypto_int64 f9g9_48 = f9   * (crypto_int64) g9_48;
    
    crypto_int64 h0 = f0g0+f1g9_48+f2g8_48+f3g7_48+f4g6_48+f5g5_48+f6g4_48+f7g3_48+f8g2_48+f9g1_48;
    crypto_int64 h1 = f0g1+f1g0   +f2g9_48+f3g8_48+f4g7_48+f5g6_48+f6g5_48+f7g4_48+f8g3_48+f9g2_48;
    crypto_int64 h2 = f0g2+f1g1   +f2g0   +f3g9_48+f4g8_48+f5g7_48+f6g6_48+f7g5_48+f8g4_48+f9g3_48;
    crypto_int64 h3 = f0g3+f1g2   +f2g1   +f3g0   +f4g9_48+f5g8_48+f6g7_48+f7g6_48+f8g5_48+f9g4_48;
    crypto_int64 h4 = f0g4+f1g3   +f2g2   +f3g1   +f4g0   +f5g9_48+f6g8_48+f7g7_48+f8g6_48+f9g5_48;
    crypto_int64 h5 = f0g5+f1g4   +f2g3   +f3g2   +f4g1   +f5g0   +f6g9_48+f7g8_48+f8g7_48+f9g6_48;
    crypto_int64 h6 = f0g6+f1g5   +f2g4   +f3g3   +f4g2   +f5g1   +f6g0   +f7g9_48+f8g8_48+f9g7_48;
    crypto_int64 h7 = f0g7+f1g6   +f2g5   +f3g4   +f4g3   +f5g2   +f6g1   +f7g0   +f8g9_48+f9g8_48;
    crypto_int64 h8 = f0g8+f1g7   +f2g6   +f3g5   +f4g4   +f5g3   +f6g2   +f7g1   +f8g0   +f9g9_48;
    crypto_int64 h9 = f0g9+f1g8   +f2g7   +f3g6   +f4g5   +f5g4   +f6g3   +f7g2   +f8g1   +f9g0   ;
    
    crypto_int64 carry;
    
    carry = h0 >> 27; h1 += carry; h0 -= (carry << 27);
    carry = h1 >> 27; h2 += carry; h1 -= (carry << 27);
    carry = h2 >> 27; h3 += carry; h2 -= (carry << 27);
    carry = h3 >> 27; h4 += carry; h3 -= (carry << 27);
    carry = h4 >> 27; h5 += carry; h4 -= (carry << 27);
    carry = h5 >> 27; h6 += carry; h5 -= (carry << 27);
    carry = h6 >> 27; h7 += carry; h6 -= (carry << 27);
    carry = h7 >> 27; h8 += carry; h7 -= (carry << 27);
    carry = h8 >> 27; h9 += carry; h8 -= (carry << 27);
    carry = h9 >> 23; h0 += carry *3; h9 -= (carry << 23);
    carry = h0 >> 27; h1 += carry; h0 -= (carry << 27);
    
    h[0] = (crypto_int32) h0;
    h[1] = (crypto_int32) h1;
    h[2] = (crypto_int32) h2;
    h[3] = (crypto_int32) h3;
    h[4] = (crypto_int32) h4;
    h[5] = (crypto_int32) h5;
    h[6] = (crypto_int32) h6;
    h[7] = (crypto_int32) h7;
    h[8] = (crypto_int32) h8;
    h[9] = (crypto_int32) h9;
}
