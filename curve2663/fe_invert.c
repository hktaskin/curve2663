//
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 28.10.2018.
//  Copyright © 2018 HKT. All rights reserved.
//  This code is mostly taken from the ref10 version of Curve25519 in SUPERCOP
//  (http://bench.cr.yp.to/supercop.html). which is released as public domain
//  https://github.com/skarati/Kummer_Line/blob/master/KummerLine2663(278,67)/kummer.h#L336
//
//
#include "fe.h"
#include <stdio.h>

// Compute z^-1 = z^(p-2) = z^(2^266-5)
void fe_invert(fe out,fe z)
{
    fe t0;
    fe t1;
    fe t2;
    fe t3;
    fe t4;
    fe t5;
    fe t6;
    fe t7;
    fe t8;
    fe t9;
    fe t10;
    fe t11;
    fe t12;
    int i;
    
    /* 2  */		fe_sq(t0,z);
    /*2^2-2^0*/		fe_mul(t1, t0, z);
    /*2^3-2^1*/		fe_sq(t2,t1);
    /*2^4-2^2*/		fe_sq(t3,t2);
    /*2^4-2^0*/		fe_mul(t3, t3, t1);

    /*2^5-2^1*/		fe_sq(t4,t3);
    /*2^7-2^3*/		for (i = 0;i < 2;i++) fe_sq(t4,t4);
    /*2^8-2^4*/		fe_sq(t5,t4);
    /*2^8-2^0*/		fe_mul(t5, t5, t3);

    /*2^9-2^1*/		fe_sq(t6,t5);
    /*2^16-2^8*/	for (i = 0;i < 7;i++) fe_sq(t6,t6);
    /*2^16-2^0*/	fe_mul(t6, t6, t5);

    /*2^17-2^1*/	fe_sq(t7,t6);
    /*2^32-2^16*/	for (i = 0;i < 15;i++) fe_sq(t7,t7);
    /*2^32-2^0*/	fe_mul(t7, t7, t6);

    /*2^33-2^1*/	fe_sq(t8,t7);
    /*2^64-2^32*/	for (i = 0;i < 31;i++) fe_sq(t8,t8);
    /*2^64-2^0*/	fe_mul(t8, t8, t7);

    /*2^65-2^1*/	fe_sq(t9,t8);
    /*2^128-2^64*/	for (i = 0;i < 63;i++) fe_sq(t9,t9);
    /*2^128-2^0*/	fe_mul(t9, t9, t8);

    /*2^129-2^1*/	fe_sq(t10,t9);
    /*2^256-2^128*/	for (i = 0;i < 127;i++) fe_sq(t10,t10);
    /*2^256-2^0*/		fe_mul(t10, t10, t9);

    /*2^257-2^1*/	fe_sq(t11,t10);
    /*2^263-2^7*/	for (i = 0;i < 6;i++) fe_sq(t11,t11);

    /*2^263-2^3*/	fe_mul(t12, t11, t4);
    /*2^263-2^1*/	fe_mul(t12, t12, t2);
    /*2^263-2^0*/	fe_mul(t12, t12, z);
    /*2^266-2^3*/	for (i = 0;i < 3;i++) fe_sq(t12,t12);

    /*2^266-5*/     fe_mul(out, t12, t1);
        
    return;
}
