//
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 28.10.2018.
//  Copyright © 2018 HKT. All rights reserved.
//  This code is mostly taken from the ref10 version of Curve25519 in SUPERCOP
//  (http://bench.cr.yp.to/supercop.html). which is released as public domain
//
//
#include "fe.h"

void fe_tobytes(unsigned char *s,fe h)
{
    crypto_int32 h0 = h[0];
    crypto_int32 h1 = h[1];
    crypto_int32 h2 = h[2];
    crypto_int32 h3 = h[3];
    crypto_int32 h4 = h[4];
    crypto_int32 h5 = h[5];
    crypto_int32 h6 = h[6];
    crypto_int32 h7 = h[7];
    crypto_int32 h8 = h[8];
    crypto_int32 h9 = h[9];
    
    crypto_int32 carry0;
    crypto_int32 carry1;
    crypto_int32 carry2;
    crypto_int32 carry3;
    crypto_int32 carry4;
    crypto_int32 carry5;
    crypto_int32 carry6;
    crypto_int32 carry7;
    crypto_int32 carry8;
    crypto_int32 carry9;
    
    carry0 = (h0 >> 27); h1 += carry0; h0 -= (carry0 << 27);
    carry1 = (h1 >> 27); h2 += carry1; h1 -= (carry1 << 27);
    carry2 = (h2 >> 27); h3 += carry2; h2 -= (carry2 << 27);
    carry3 = (h3 >> 27); h4 += carry3; h3 -= (carry3 << 27);
    carry4 = (h4 >> 27); h5 += carry4; h4 -= (carry4 << 27);
    carry5 = (h5 >> 27); h6 += carry5; h5 -= (carry5 << 27);
    carry6 = (h6 >> 27); h7 += carry6; h6 -= (carry6 << 27);
    carry7 = (h7 >> 27); h8 += carry7; h7 -= (carry7 << 27);
    carry8 = (h8 >> 27); h9 += carry8; h8 -= (carry8 << 27);
    carry9 = (h9 >> 23); h0 += carry9 * 3; h9 -= (carry9 << 23);
    
    s[0] = h0 & 0xff;               /*8*/
    h0 = h0>>8;
    s[1] = h0 & 0xff;               /*16*/
    h0 = h0>>8;
    s[2] = h0 & 0xff;               /*24*/
    h0 = h0>>8;
    s[3] = h0 & 0x7;                /*27*/
    
    s[3] = s[3]|(h1 & 0x1f) << 3;	/*5*/
    h1 = h1 >> 5;
    s[4] = h1 & 0xff;               /*13*/
    h1 = h1>>8;
    s[5] = h1 & 0xff;               /*21*/
    h1 = h1>>8;
    s[6] = h1 & 0x3f;               /*27*/
    
    s[6] = s[6]|(h2 & 0x3) << 6;	/*2*/
    h2 = h2 >> 2;
    s[7] = h2 & 0xff;               /*10*/
    h2 = h2>>8;
    s[8] = h2 & 0xff;               /*18*/
    h2 = h2>>8;
    s[9] = h2 & 0xff;               /*26*/
    h2 = h2>>8;
    s[10] = h2 & 0x1;               /*27*/
    
    s[10] = s[10]|(h3 & 0x7f) << 1;	/*7*/
    h3 = h3 >> 7;
    s[11] = h3 & 0xff;              /*15*/
    h3 = h3>>8;
    s[12] = h3 & 0xff;              /*23*/
    h3 = h3>>8;
    s[13] = h3 & 0xf;               /*27*/
    
    s[13] = s[13]|(h4 & 0xf) << 4;	/*4*/
    h4 = h4 >> 4;
    s[14] = h4 & 0xff;              /*12*/
    h4 = h4>>8;
    s[15] = h4 & 0xff;              /*20*/
    h4 = h4>>8;
    s[16] = h4 & 0x7f;              /*27*/
    
    s[16] = s[16]|(h5 & 0x1) << 7;	/*1*/
    h5 = h5 >> 1;
    s[17] = h5 & 0xff;              /*9*/
    h5 = h5 >> 8;
    s[18] = h5 & 0xff;              /*17*/
    h5 = h5>>8;
    s[19] = h5 & 0xff;              /*25*/
    h5 = h5>>8;
    s[20] = h5 & 0x3;               /*27*/
    
    s[20] = s[20]|(h6 & 0x3f) << 2;	/*6*/
    h6 = h6 >> 6;
    s[21] = h6 & 0xff;              /*14*/
    h6 = h6>>8;
    s[22] = h6 & 0xff;              /*22*/
    h6 = h6>>8;
    s[23] = h6 & 0x1f;              /*27*/
    
    s[23] = s[23]|(h7 & 0x7) << 5;	/*3*/
    h7 = h7 >> 3;
    s[24] = h7 & 0xff;              /*11*/
    h7 = h7 >> 8;
    s[25] = h7 & 0xff;              /*19*/
    h7 = h7>>8;
    s[26] = h7 & 0xff;              /*27*/
    
    s[27] = h8 & 0xff;              /*8*/
    h8 = h8>>8;
    s[28] = h8 & 0xff;              /*16*/
    h8 = h8>>8;
    s[29] = h8 & 0xff;              /*24*/
    h8 = h8>>8;
    s[30] = h8 & 0x7;               /*27*/
    
    s[30] = s[30]|(h9 & 0x1f) << 3;	/*5*/
    h9 = h9 >> 5;
    s[31] = h9 & 0xff;              /*13*/
    h9 = h9>>8;
    s[32] = h9 & 0xff;              /*21*/
    h9 = h9>>8;
    s[33] = h9 & 0x3;               /*23*/
}
