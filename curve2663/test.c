//
//  test.c
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 3.11.2018.
//  Copyright © 2018 HKT. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include "fe.h"
#include "scalarmult.h"
#include "test.h"
#include "helpers.h"
#include <time.h>

void test_from_to_bytes(int randomize)
{
    fe x1;

    static unsigned char *x1frombytes;
    static unsigned char *x1tobytes;

    
    int seed = randomize ? rand() + getpid() + (int)time(NULL) : 1;
    srandom(seed);
    printf("test_from_to_bytes # seed = %d\n",seed);
    
    x1frombytes = alignedcalloc(CRYPTO_SCALARBYTES);
    x1tobytes = alignedcalloc(CRYPTO_SCALARBYTES);

    fillrandombytes(x1frombytes, CRYPTO_SCALARBYTES);
    x1frombytes[33] &= 0x03;
    
    printf("x1frombytes: "); printarray(x1frombytes,CRYPTO_SCALARBYTES);
    
    fe_frombytes(x1,x1frombytes);
    
    fe_tobytes(x1tobytes, x1); printf("x1tobytes  : "); printarray(x1tobytes,CRYPTO_SCALARBYTES);
    printf("             "); comparearray(x1frombytes, x1tobytes, CRYPTO_SCALARBYTES);

    printf("x1: "); printfe(x1);
    printf("    "); fecheck(x1);
    
    
}

void test_fe_mul5177(int randomize)
{
    static unsigned char *eltbytes;
    static unsigned char *elt5177bytes;
    fe elt;
    fe elt5177;
    
    int seed = randomize ? rand() + getpid() + (int)time(NULL) : 1 ;
    srandom(seed);
    printf("test_fe_mul5177 # seed = %d\n",seed);
    
    eltbytes = alignedcalloc(CRYPTO_SCALARBYTES);
    elt5177bytes = alignedcalloc(CRYPTO_SCALARBYTES);
    
    fillrandombytes(eltbytes,CRYPTO_SCALARBYTES);
    printf("eltbytes:     "); printarray(eltbytes,CRYPTO_SCALARBYTES);

    fe_frombytes(elt,eltbytes);
    printf("elt:          "); printfe(elt);
    
    fe_mul5177(elt5177,elt);
    
    printf("elt5177:      "); printfe(elt5177);
    printf("check:        "); fecheck(elt5177);
    
    fe_tobytes(elt5177bytes, elt5177); printf("elt5177bytes: "); printarray(elt5177bytes,CRYPTO_SCALARBYTES);
}

void test_fe_mul(int randomize)
{
    fe x1;
    fe x2;
    fe x1x2;
    fe x2x1;
    
    static unsigned char *x1bytes;
    static unsigned char *x2bytes;
    static unsigned char *x1x2bytes;
    static unsigned char *x2x1bytes;
    
    int seed = randomize ? rand() + getpid() + (int)time(NULL) : 1;
    srandom(seed);
    printf("test_fe_mul # fe_mul: ");
    
#ifdef FE_MUL_TYPE_REF10
    printf("REF10");
#elif defined FE_MUL_TYPE_DONNA
    printf("DONNA");
#elif defined FE_MUL_TYPE_KUMMER
    printf("KUMMER");
#elif defined FE_MUL_TYPE_TMVP
    #ifdef FE_MUL_TYPE_TMVP_TMVP
        printf("TMVP_TMVP");
    #elif defined FE_MUL_TYPE_TMVP_SCHOOLBOOK
        printf("TMVP_SCHOOLBOOK");
    #endif
#endif
    
    printf(" # seed: %d\n",seed);
    
    x1bytes = alignedcalloc(CRYPTO_SCALARBYTES); fillrandombytes(x1bytes, CRYPTO_SCALARBYTES); x1bytes[33] &= 0x00; x1bytes[32] &= 0x03;
    x2bytes = alignedcalloc(CRYPTO_SCALARBYTES); fillrandombytes(x2bytes, CRYPTO_SCALARBYTES); x2bytes[33] &= 0x00; x2bytes[32] &= 0x03;
    x1x2bytes = alignedcalloc(CRYPTO_SCALARBYTES);
    x2x1bytes = alignedcalloc(CRYPTO_SCALARBYTES);
    
    printf("x1bytes: "); printarray(x1bytes,CRYPTO_SCALARBYTES);
    printf("x2bytes: "); printarray(x2bytes,CRYPTO_SCALARBYTES);
    
    fe_frombytes(x1,x1bytes); /*fe_11(x1);*/ printf("x1: "); printfe(x1);
    fe_frombytes(x2,x2bytes); /*fe_1(x2); */ printf("x2: "); printfe(x2);
    
    printf("       h0       h1       h2       h3       h4       h5       h6       h7       h8       h9\n");
    fe_mul(x1x2,x1,x2); printf("x1x2:  "); printfe(x1x2);
    printf("       "); fecheck(x1x2);
    fe_mul(x2x1,x2,x1); printf("x2x1:  "); printfe(x2x1);
    printf("       "); fecheck(x2x1);
    
    fe_tobytes(x1x2bytes, x1x2); printf("x1x2 bytes: "); printarray(x1x2bytes,CRYPTO_SCALARBYTES);
    fe_tobytes(x2x1bytes, x2x1); printf("x2x1 bytes: "); printarray(x2x1bytes,CRYPTO_SCALARBYTES);
    printf("Comparison: "); comparearray(x1x2bytes, x2x1bytes, CRYPTO_SCALARBYTES);
}

void test_fe_sq(int randomize)
{
    fe x1;
    fe x1x1;
    fe x1sq;
    
    static unsigned char *x1bytes;
    static unsigned char *x1x1bytes;
    static unsigned char *x1sqbytes;
    
    int seed = randomize ? rand() + getpid() + (int)time(NULL) : 1;
    srandom(seed);
    printf("test_fe_sq # fe_mul: ");
    
#ifdef FE_MUL_TYPE_REF10
    printf("REF10");
#elif defined FE_MUL_TYPE_DONNA
    printf("DONNA");
#elif defined FE_MUL_TYPE_KUMMER
    printf("KUMMER");
#elif defined FE_MUL_TYPE_TMVP
#ifdef FE_MUL_TYPE_TMVP_TMVP
    printf("TMVP_TMVP");
#elif defined FE_MUL_TYPE_TMVP_SCHOOLBOOK
    printf("TMVP_SCHOOLBOOK");
#endif
#endif
    
    printf(" # seed = %d\n",seed);
    
    x1bytes = alignedcalloc(CRYPTO_SCALARBYTES); fillrandombytes(x1bytes, CRYPTO_SCALARBYTES); x1bytes[33] &= 0x00; x1bytes[32] &= 0x03;
    x1x1bytes = alignedcalloc(CRYPTO_SCALARBYTES);
    x1sqbytes = alignedcalloc(CRYPTO_SCALARBYTES);
    
    printf("x1bytes: "); printarray(x1bytes,CRYPTO_SCALARBYTES);
    
    fe_frombytes(x1,x1bytes); /*fe_1(x1);*/ printf("x1:    "); printfe(x1);
    
    printf("       h0       h1       h2       h3       h4       h5       h6       h7       h8       h9\n");
    fe_mul(x1x1,x1,x1); printf("x1x1:  "); printfe(x1x1);
    printf("       "); fecheck(x1x1);
    fe_sq(x1sq,x1); printf("x1sq:  "); printfe(x1sq);
    printf("       "); fecheck(x1sq);
    
    fe_tobytes(x1x1bytes, x1x1); printf("x1x1 bytes: "); printarray(x1x1bytes,CRYPTO_SCALARBYTES);
    fe_tobytes(x1sqbytes, x1sq); printf("x1sq bytes: "); printarray(x1sqbytes,CRYPTO_SCALARBYTES);
    printf("Comparison: "); comparearray(x1x1bytes, x1sqbytes, CRYPTO_SCALARBYTES);
}

void test_fe_invert(int randomize)
{
    fe x1;
    fe x1invert;
    fe x1restored;
    fe x1sq;
    
    static unsigned char *x1bytes;
    static unsigned char *x1invertbytes;
    static unsigned char *x1restoredbytes;
    
    int seed = randomize ? rand() + getpid() + (int)time(NULL) : 1;
    srandom(seed);
    printf("test_fe_invert # fe_mul: ");

#ifdef FE_MUL_TYPE_REF10
    printf("REF10");
#elif defined FE_MUL_TYPE_DONNA
    printf("DONNA");
#elif defined FE_MUL_TYPE_KUMMER
    printf("KUMMER");
#elif defined FE_MUL_TYPE_TMVP
#ifdef FE_MUL_TYPE_TMVP_TMVP
    printf("TMVP_TMVP");
#elif defined FE_MUL_TYPE_TMVP_SCHOOLBOOK
    printf("TMVP_SCHOOLBOOK");
#endif
#endif
    
    printf(" # seed = %d\n",seed);
    
    x1bytes = alignedcalloc(CRYPTO_SCALARBYTES); fillrandombytes(x1bytes, CRYPTO_SCALARBYTES); x1bytes[33] &= 0x00; x1bytes[32] &= 0x03;
    x1invertbytes = alignedcalloc(CRYPTO_SCALARBYTES);
    x1restoredbytes = alignedcalloc(CRYPTO_SCALARBYTES);
    
    printf("x1bytes: "); printarray(x1bytes,CRYPTO_SCALARBYTES);
//    printf("            h9       h8       h7       h6       h5       h4       h3       h2       h1       h0\n");
    printf("            h0       h1       h2       h3       h4       h5       h6       h7       h8       h9\n");

    fe_frombytes(x1,x1bytes); /*fe_1(x1);*/ printf("x1:         "); printfe(x1);
    printf("            "); fecheck(x1);
    
    fe_invert(x1invert,x1); printf("x1invert:   "); printfe(x1invert);
    printf("            "); fecheck(x1invert);

    fe_mul(x1restored,x1invert,x1); printf("x1*x1inv:   "); printfe(x1restored);
    printf("            "); fecheck(x1restored);
    
    fe_sq(x1sq, x1);
    fe_mul(x1restored,x1invert,x1sq); printf("x1sq*inv:   "); printfe(x1restored);
    printf("            "); fecheck(x1restored);
    
    //fe_tobytes(x1restoredbytes, x1restored); printf("x1*x1inv:   "); printarray(x1restoredbytes,CRYPTO_SCALARBYTES);
    //printf("x1 vx x1res: "); comparearray(x1restoredbytes, x1bytes, CRYPTO_SCALARBYTES);
}

void test_ecdh(int randomize)
{
    static unsigned char *sA;
    static unsigned char *sB;
    static unsigned char *pA;
    static unsigned char *pB;
    static unsigned char *kA;
    static unsigned char *kB;
    
    int seed = randomize ? rand() + getpid() + (int)time(NULL) : 1;
    srandom(seed);
    printf("test_ecdh # seed = %d\n",seed);
    
    sA = alignedcalloc(CRYPTO_SCALARBYTES); fillrandombytes(sA,CRYPTO_SCALARBYTES); printf("sA: "); printarray(sA,CRYPTO_SCALARBYTES);
    sB = alignedcalloc(CRYPTO_SCALARBYTES); fillrandombytes(sB,CRYPTO_SCALARBYTES); printf("sB: "); printarray(sB,CRYPTO_SCALARBYTES);
    pA = alignedcalloc(CRYPTO_SCALARBYTES);
    pB = alignedcalloc(CRYPTO_SCALARBYTES);
    kA = alignedcalloc(CRYPTO_SCALARBYTES);
    kB = alignedcalloc(CRYPTO_SCALARBYTES);
    
    // Q = basepoint * P;
    //crypto_scalarmult_base(Q,P);
    crypto_scalarmult_base(pA,sA); printf("pA: "); printarray(pA,CRYPTO_SCALARBYTES);
    crypto_scalarmult_base(pB,sB); printf("pB: "); printarray(pB,CRYPTO_SCALARBYTES);
    
    // Q = n * P;
    // crypto_scalarmult(Q,n,P);
    crypto_scalarmult(kA,sA,pB); printf("kA: "); printarray(kA,CRYPTO_SCALARBYTES);
    crypto_scalarmult(kB,sB,pA); printf("kB: "); printarray(kB,CRYPTO_SCALARBYTES);
    printf("    "); comparearray(kA, kB, CRYPTO_SCALARBYTES);

}
/*
 Sample Outout for Curve25519
 srandom(0xD3C0D3);
 sA: E5 5A 1A 4B DC 2F 2A 06 A7 F1 34 6A 77 D8 45 BA 51 C2 F8 51 0A EA 83 9D 53 B4 BE 29 BF 1B 79 A4
 sB: 18 DE 46 1D E8 E6 EF 72 BA 53 08 4E E6 3B 0A D5 D9 87 EE 1C EF 1A 95 76 7C 1A 15 36 27 70 67 40
 pA: 1D 98 DA 2E 55 65 54 57 BC F9 40 EE 9B 6E 08 5D 55 A8 BA 59 1D 98 5E FB E2 97 4E A3 06 82 C0 05
 pB: E6 4A 89 1D 47 05 20 56 C1 9C 2D F1 64 EF 4F BB 66 D8 C8 07 11 BA 84 BB FF 86 6E 69 58 B9 01 21
 kA: D3 C4 7F D8 26 60 DD 92 67 42 11 73 BC 50 3D FF F4 3C 42 AE 0A 9D BE BF CA F1 27 5A C0 28 01 78
 kB: D3 C4 7F D8 26 60 DD 92 67 42 11 73 BC 50 3D FF F4 3C 42 AE 0A 9D BE BF CA F1 27 5A C0 28 01 78
 */

