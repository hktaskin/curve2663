//
//  test.h
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 3.11.2018.
//  Copyright © 2018 HKT. All rights reserved.
//

#ifndef test_h
#define test_h

#define RANDOMIZE 1
#define NOT_RANDOMIZE 0

void test_from_to_bytes(int randomize);

void test_fe_mul5177(int randomize);

void test_fe_mul(int randomize);

void test_fe_sq(int randomize);

void test_fe_invert(int randomize);

void test_ecdh(int randomize);

void test_cycles_fe_mul(int randomize);

void test_cycles_fe_invert(int randomize);

void test_cycles_ecdh(int randomize);

#endif /* test_h */
