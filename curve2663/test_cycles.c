//
//  test_cycles.c
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 6.11.2018.
//  Copyright © 2018 HKT. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include "fe.h"
#include "scalarmult.h"
#include "helpers.h"
#include <time.h>
#include "measurements.h"

void test_cycles_fe_mul(int randomize)
{
    fe x1;
    fe x2;
    fe x1x2;
    
    static unsigned char *x1bytes;
    static unsigned char *x2bytes;
    static unsigned char *x1x2bytes;
    
    int seed = randomize ? rand() + getpid() + (int)time(NULL) : 1;
    srandom(seed);
    printf("test_cycles_fe_mul # fe_mul: ");
    
#ifdef FE_MUL_TYPE_REF10
    printf("REF10");
#elif defined FE_MUL_TYPE_DONNA
    printf("DONNA");
#elif defined FE_MUL_TYPE_KUMMER
    printf("KUMMER");
#elif defined FE_MUL_TYPE_TMVP
#ifdef FE_MUL_TYPE_TMVP_TMVP
    printf("TMVP_TMVP");
#elif defined FE_MUL_TYPE_TMVP_SCHOOLBOOK
    printf("TMVP_SCHOOLBOOK");
#endif
#endif
    
    printf(" # seed: %d\n",seed);
    
    x1bytes = alignedcalloc(CRYPTO_SCALARBYTES); fillrandombytes(x1bytes, CRYPTO_SCALARBYTES); x1bytes[33] &= 0x00; x1bytes[32] &= 0x03;
    x2bytes = alignedcalloc(CRYPTO_SCALARBYTES); fillrandombytes(x2bytes, CRYPTO_SCALARBYTES); x2bytes[33] &= 0x00; x2bytes[32] &= 0x03;
    x1x2bytes = alignedcalloc(CRYPTO_SCALARBYTES);
    
    printf("x1bytes:  "); printarray(x1bytes,CRYPTO_SCALARBYTES);
    printf("x2bytes:  "); printarray(x2bytes,CRYPTO_SCALARBYTES);

    printf("          h0       h1       h2       h3       h4       h5       h6       h7       h8       h9\n");
    fe_frombytes(x1,x1bytes); printf("x1:       "); printfe(x1);
    fe_frombytes(x2,x2bytes); printf("x2:       "); printfe(x2);
    
    printf("          Started to measure...\n");
    MEASURE(
        fe_mul(x1x2,x1,x2);
    );
    printf("          Done. \n");
    
    printf("x1x2:     "); printfe(x1x2);
    printf("          "); fecheck(x1x2);
    
    fe_tobytes(x1x2bytes, x1x2); printf("x1x2bytes:"); printarray(x1x2bytes,CRYPTO_SCALARBYTES);
    
    printf("Total CPU cycles for fe_mul: %.2f.\n", RDTSC_total_clk);

}

void test_cycles_fe_invert(int randomize)
{
    fe x1;
    fe x1inv;
    
    static unsigned char *x1bytes;
    
    int seed = randomize ? rand() + getpid() + (int)time(NULL) : 1;
    srandom(seed);
    printf("test_cycles_fe_invert # fe_mul: ");
    
#ifdef FE_MUL_TYPE_REF10
    printf("REF10");
#elif defined FE_MUL_TYPE_DONNA
    printf("DONNA");
#elif defined FE_MUL_TYPE_KUMMER
    printf("KUMMER");
#elif defined FE_MUL_TYPE_TMVP
#ifdef FE_MUL_TYPE_TMVP_TMVP
    printf("TMVP_TMVP");
#elif defined FE_MUL_TYPE_TMVP_SCHOOLBOOK
    printf("TMVP_SCHOOLBOOK");
#endif
#endif
    
    printf(" # seed: %d\n",seed);
    
    x1bytes = alignedcalloc(CRYPTO_SCALARBYTES); fillrandombytes(x1bytes, CRYPTO_SCALARBYTES); x1bytes[33] &= 0x00; x1bytes[32] &= 0x03;
    
    printf("x1bytes:  "); printarray(x1bytes,CRYPTO_SCALARBYTES);
    
    printf("          h0       h1       h2       h3       h4       h5       h6       h7       h8       h9\n");
    fe_frombytes(x1,x1bytes); printf("x1:       "); printfe(x1);
    
    printf("          Started to measure...\n");
    MEASURE(
            fe_invert(x1inv,x1);
            );
    printf("          Done. \n");
    
    printf("x1inv:    "); printfe(x1inv);
    
    printf("Total CPU cycles for fe_invert: %.2f.\n", RDTSC_total_clk);
    
}


void test_cycles_ecdh(int randomize)
{
    static unsigned char *sA;
    static unsigned char *pA;
    static unsigned char *kA;
    
    int seed = randomize ? rand() + getpid() + (int)time(NULL) : 1;
    srandom(seed);
    printf("test_cycles_ecdh # fe_mul: ");
    
#ifdef FE_MUL_TYPE_REF10
    printf("REF10");
#elif defined FE_MUL_TYPE_DONNA
    printf("DONNA");
#elif defined FE_MUL_TYPE_KUMMER
    printf("KUMMER");
#elif defined FE_MUL_TYPE_TMVP
#ifdef FE_MUL_TYPE_TMVP_TMVP
    printf("TMVP_TMVP");
#elif defined FE_MUL_TYPE_TMVP_SCHOOLBOOK
    printf("TMVP_SCHOOLBOOK");
#endif
#endif

    printf(" # seed: %d\n",seed);
    
    sA = alignedcalloc(CRYPTO_SCALARBYTES); fillrandombytes(sA,CRYPTO_SCALARBYTES); printf("sA: "); printarray(sA,CRYPTO_SCALARBYTES);
    pA = alignedcalloc(CRYPTO_SCALARBYTES);
    kA = alignedcalloc(CRYPTO_SCALARBYTES);
    
    // Q = basepoint * P;
    //crypto_scalarmult_base(Q,P);
    
    printf("Started to measure: crypto_scalarmult_base\n");
    MEASURE(
            crypto_scalarmult_base(pA,sA);
            );
    printf(" Done. \n");
    printf(" Total CPU cycles for crypto_scalarmult_base: %.2f.\n", RDTSC_total_clk);
    
    
    //printf("pA: "); printarray(pA,CRYPTO_SCALARBYTES);
    
    // Q = n * P;
    // crypto_scalarmult(Q,n,P);
    
    printf("Started to measure: crypto_scalarmult\n");
    MEASURE(
            crypto_scalarmult(kA,sA,pA);
            );
    printf(" Done. \n");
    printf(" Total CPU cycles for crypto_scalarmult: %.2f.\n", RDTSC_total_clk);
    
    //printf("kA: "); printarray(kA,CRYPTO_SCALARBYTES);
    
}
