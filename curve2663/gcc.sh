#!/bin/bash
rm 0*
gcc -O3 -ffast-math -fno-common -D FE_MUL_TYPE_REF10 -o 01_ref10 *.c
gcc -O3 -ffast-math -fno-common -D FE_MUL_TYPE_DONNA -o 02_donna *.c
gcc -O3 -ffast-math -fno-common -D FE_MUL_TYPE_KUMMER -o 03_kummer *.c
gcc -O3 -ffast-math -fno-common -D FE_MUL_TYPE_TMVP -D FE_MUL_TYPE_TMVP_TMVP -o 04_tmvp_tmvp *.c
gcc -O3 -ffast-math -fno-common -D FE_MUL_TYPE_TMVP -D FE_MUL_TYPE_TMVP_SCHOOLBOOK -o 05_tmvp_sb *.c
./01_ref10
./02_donna
./03_kummer
./04_tmvp_tmvp
./05_tmvp_sb
