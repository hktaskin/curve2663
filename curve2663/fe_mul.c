//
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 28.10.2018.
//  Copyright © 2018 HKT. All rights reserved.
//  This code is mostly taken from the ref10 version of Curve25519 in SUPERCOP
//  (http://bench.cr.yp.to/supercop.html). which is released as public domain
//
//
#include <stdio.h>
#include "fe.h"

void fe_mul(fe h,fe f,fe g)
{
#ifdef FE_MUL_TYPE_REF10
    fe_mul_ref10(h,f,g);
#elif defined FE_MUL_TYPE_DONNA
    fe_mul_donna(h,f,g);
#elif defined FE_MUL_TYPE_KUMMER
    fe_mul_kummer(h,f,g);
#elif defined FE_MUL_TYPE_TMVP
    fe_mul_tmvp(h,f,g);
#endif
}
