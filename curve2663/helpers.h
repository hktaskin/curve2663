//
//  helpers.h
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 3.11.2018.
//  Copyright © 2018 HKT. All rights reserved.
//

#ifndef helpers_h
#define helpers_h

#include "fe.h"

#define CRYPTO_SCALARBYTES 34

void fail(const char *why);

void printarray(const unsigned char *arr, int len);

void comparearray(const unsigned char *arr1,const unsigned char *arr2, int len);

void printfe(fe elt);

//void printfe_inverse(fe elt);

void fecheck(fe elt);

void fillrandombytes(unsigned char *x,unsigned long long xlen);

unsigned char *alignedcalloc(unsigned long long len);


#endif /* helpers_h */
