//
//  curve2663
//
//  Created by Halil Kemal TAŞKIN on 28.10.2018.
//  Copyright © 2018 HKT. All rights reserved.
//  This code is mostly taken from the ref10 version of Curve25519 in SUPERCOP
//  (http://bench.cr.yp.to/supercop.html). which is released as public domain
//
//
#ifndef FE_H
#define FE_H

#include "crypto_int32.h"
#include "crypto_int64.h"

typedef crypto_int32 fe[10];
static const unsigned char basepoint[34] = {17};

static const crypto_int64 cmask27 = 0x7ffffff;
static const crypto_int64 cmask23 = 0x7fffff;

//#define FE_MUL_TYPE_REF10
//#define FE_MUL_TYPE_DONNA
//#define FE_MUL_TYPE_KUMMER
//#define FE_MUL_TYPE_TMVP

//#define FE_MUL_TYPE_TMVP_TMVP
//#define FE_MUL_TYPE_TMVP_SCHOOLBOOK

/*
fe means field element.
Here the field is \Z/(2^266-3).
An element t, entries t[0]...t[9], represents the integer
t[0]+2^27 t[1]+2^54 t[2]+2^81 t[3]+2^108 t[4]+...+2^243 t[9].
Bounds on each t[i] vary depending on context.
*/

extern void fe_frombytes(fe,const unsigned char *);
extern void fe_tobytes(unsigned char *,fe);

extern void fe_copy(fe,fe);
extern void fe_0(fe);
extern void fe_1(fe);
extern void fe_11(fe);
extern void fe_cswap(fe,fe,unsigned int);

extern void fe_add(fe,fe,fe);
extern void fe_sub(fe,fe,fe);

extern void fe_mul(fe,fe,fe);
extern void fe_mul_ref10(fe,fe,fe);
extern void fe_mul_donna(fe,fe,fe);
extern void fe_mul_kummer(fe h,fe f,fe g);
extern void fe_mul_tmvp(fe,fe,fe);

extern void fe_sq(fe,fe);
extern void fe_mul5177(fe,fe);
extern void fe_invert(fe,fe);

#endif
